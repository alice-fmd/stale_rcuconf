// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Config.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Config table class
*/
#include "Config.h"
#include "Sequence.h"
#include <rcudb/Row.h>
#include <rcudb/Result.h>
#include <rcudb/Server.h>
#include <rcudb/Sql.h>
#include <iostream>

const std::string RcuConf::Config::fgName("Config");

//_____________________________________________________________________
RcuConf::Config::Config(RcuDb::Row& row) 
  : Table(row)
{
  row.Field(1, fTag);
  row.Field(2, fX);
  row.Field(3, fY);
  row.Field(4, fZ);
  row.Field(5, fPriorityId);
  row.Field(6, fVersion);
  fDescription = std::string(row.FieldStr(7), row.FieldLen(7));
}

//_____________________________________________________________________
void
RcuConf::Config::Print(std::ostream& o) const
{
  o << "Config: id="     << fId 
    << "\ttag="          << fTag
    << "\tx="            << fX
    << "\ty="            << fY
    << "\tz="            << fZ
    << "\tpriorityid="      << fPriorityId
    << "\tversion="      << fVersion 
    << "\tdescription='" << fDescription 
    << "'"               << std::endl;
}

//_____________________________________________________________________
bool
RcuConf::Config::Insert(RcuDb::Server& server) 
{
  // Get previous versions. 
  List vers;
  if (!Select(vers, server, fTag, fX, fY, fZ)) return false;

  // If we have other entries with the same tag, auto-update the
  // version number 
  if (vers.size() <= 0)  fVersion = 0;
  else {
    Config* c = *(vers.begin());
    fVersion  = c->fVersion + 1;
  }

  // Get a unique ID from the sequence table 
  if (!MakeId(server)) return false;

  // And then do the insert. 
  RcuDb::Sql sql2;
  sql2 << "INSERT INTO " << fgName << " VALUES(";
  IdOut(sql2) << fTag         << "," 
	      << fX           << "," 
	      << fY           << "," 
	      << fZ           << "," 
	      << fPriorityId     << ","
	      << fVersion     << ",'" 
	      << fDescription << "')";
  if (!Dump(sql2)) return true;
  return server.Exec(sql2);
}


//_____________________________________________________________________
bool
RcuConf::Config::Create(RcuDb::Server& server) 
{
  RcuDb::Sql sql;
  sql << "CREATE TABLE " << fgName << " ("
      << "  id          INT KEY, " 
      << "  tag         INT NOT NULL, " 
      << "  x           INT NOT NULL, "
      << "  y           INT NOT NULL, "
      << "  z           INT NOT NULL, "
      << "  priorityid     INT NOT NULL, "
      << "  version     INT NOT NULL, " 
      << "  description TEXT, "
      << "  INDEX(tag), "
      << "  INDEX(x), "
      << "  INDEX(y), "
      << "  INDEX(z), "
      << "  INDEX(priorityid), "
      << "  INDEX(version))";
  if (!Dump(sql)) return true;
  return server.Exec(sql);
}

//_____________________________________________________________________
bool
RcuConf::Config::Drop(RcuDb::Server& server) 
{
  return Table::Drop(server, fgName);
}

//_____________________________________________________________________
bool
RcuConf::Config::Select(List& l, RcuDb::Server& server, const RcuDb::Sql& cond) 
{
  // Make the query 
  RcuDb::Sql     sql;
  sql << "SELECT * FROM " << fgName 
      << (cond.Text().empty() ? "" : " WHERE ") 
      << (cond.Text().empty() ? "" : cond);
  if (!Dump(sql)) return true;

  // Execute query, and check error status
  RcuDb::Result* res = server.Query(sql);
  if (server.IsError()) return false;

  // Make the result table if any 
  if (!res) return true;
  RcuDb::Row* row = 0;
  while ((row = res->Next())) l.push_back(new Config(*row));

  // delete result, and return OK.
  delete res;
  return true;  
}

//_____________________________________________________________________
bool
RcuConf::Config::Select(List& l, RcuDb::Server& server, int tag, 
			int x, int y, int z) 
{
  RcuDb::Sql cond;
  cond << "tag=" << tag << " AND "
       << "x="   << x   << " AND " 
       << "y="   << y   << " AND " 
       << "z="   << z   
       << " ORDER BY version DESC";
  return Select(l, server, cond);
}

//_____________________________________________________________________
//
// EOF
//


  

  
