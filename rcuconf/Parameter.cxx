// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Parameter.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Parameter table class
*/
#include "Parameter.h"
#include "Value.h"
#include "Sequence.h"
#include <iostream>
#include <rcudb/Server.h>
#include <rcudb/Result.h>
#include <rcudb/Sql.h>
#include <rcudb/Row.h>

const std::string RcuConf::Parameter::fgName("Parameter");

namespace  RcuConf 
{
  std::ostream& operator<<(std::ostream& o, const Parameter::Where w) 
  {
    switch (w) {
    case Parameter::kRcu:     o << "RCU";     break;
    case Parameter::kBc:      o << "BC";      break;
    case Parameter::kAltro:   o << "ALTRO";   break;
    case Parameter::kInvalid: o << "invalid"; break;
    }
    return o;
  }
  std::istream& operator>>(std::istream& i, Parameter::Where& w) 
  {
    std::string s;
    i >> s;
    if      (s == "RCU")    w = Parameter::kRcu;
    else if (s == "BC")     w = Parameter::kBc;
    else if (s == "ALTRO")  w = Parameter::kAltro;
    else                    w = Parameter::kInvalid;
    return i;
  }
}

//_____________________________________________________________________
RcuConf::Parameter::Parameter(RcuDb::Row& row) 
  : Table(row)
{
  fName = std::string(row.FieldStr(1), row.FieldLen(1));
  row.Field(2, fDestination);
  row.Field(3, fIsBlob);
  row.Field(4, fMask);
}

//_____________________________________________________________________
void
RcuConf::Parameter::Print(std::ostream& o) const
{
  o << "Parameter: id="		<< fId 
    << "\tname="		<< fName 
    << "\tdestination="         << fDestination
    << "\tblob=" << (fIsBlob ? "yes" : "no") 
    << "\tmask=0x" << std::hex << fMask << std::dec << std::endl;
}

//_____________________________________________________________________
bool
RcuConf::Parameter::Insert(RcuDb::Server& server) 
{
  // Get previous versions. 
  RcuDb::Sql sql1;
  sql1 << "name='"        << fName        << "' AND " 
       << "destination='" << fDestination << "' AND " 
       << "isblob="       << fIsBlob;
  List vers;
  if (!Select(vers, server, sql1)) return false;
  // We can have only one instance of a parameter
  if (vers.size() > 0) {
    Parameter* a = *(vers.begin());
    fId          = a->fId;
    return true;
  }

  // Get a unique ID from the sequence table 
  if (!MakeId(server)) return false;

  // And then do the insert. 
  RcuDb::Sql sql2;
  sql2 << "INSERT INTO " << fgName << " VALUES(";
  IdOut(sql2) << "'"
	      << fName        << "','" 
	      << fDestination << "'," 
	      << fIsBlob      << ","
	      << fMask        << ")";
  if (!Dump(sql2)) return true;
  return server.Exec(sql2);
}


//_____________________________________________________________________
bool
RcuConf::Parameter::Create(RcuDb::Server& server) 
{
  RcuDb::Sql sql;
  sql << "CREATE TABLE " << fgName << " ("
      << "  id          INT KEY, " 
      << "  name        VARCHAR(64) NOT NULL, " 
      << "  destination ENUM('RCU','BC','ALTRO') NOT NULL, "
      << "  isblob      BOOL NOT NULL, "
      << "  mask        INT NOT NULL, " 
      << "  INDEX(name), " 
      << "  INDEX(destination))";
  if (!Dump(sql)) return true;
  return server.Exec(sql);
}

//_____________________________________________________________________
bool
RcuConf::Parameter::Drop(RcuDb::Server& server) 
{
  return Table::Drop(server, fgName);
}

//_____________________________________________________________________
bool
RcuConf::Parameter::Select(List& l, RcuDb::Server& server, const RcuDb::Sql& cond) 
{
  // Make the query 
  RcuDb::Sql     sql;
  sql << "SELECT * FROM " << fgName 
      << (cond.Text().empty() ? "" : " WHERE ") 
      << (cond.Text().empty() ? "" : cond);
  if (!Dump(sql)) return true;

  // Execute query, and check error status
  RcuDb::Result* res = server.Query(sql);
  if (server.IsError()) return false;

  // Make the result table if any 
  if (!res) return true;
  RcuDb::Row* row = 0;
  while ((row = res->Next())) l.push_back(new Parameter(*row));

  // delete result, and return OK.
  delete res;
  return true;  
}

//_____________________________________________________________________
bool
RcuConf::Parameter::Select(List& l, RcuDb::Server& server, 
			   Where dest, const std::string& name)
{
  RcuDb::Sql sql;
  sql << "destination=" << dest;
  if (!name.empty()) sql << " AND name=" << name;
  return Select(l, server, sql);
}

//_____________________________________________________________________
//
// EOF
//


  

  
