// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Parameter.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Parameter table class
*/
#ifndef RCUCONF_PARAMETER_H
#define RCUCONF_PARAMETER_H
#ifndef RCUCONF_TABLE_H
# include <rcuconf/Table.h>
#endif
#ifndef __LIST__
# include <list>
#endif

namespace RcuDb 
{
  class Result;
  class Row;
  class Sql;
}
namespace RcuConf
{
  
  /** @class Parameter 
      @brief This table contains information about all possible
      parameters in the configurat 
      @ingroup Tables 
  */
  class Parameter : public Table 
  {
  public:
    /** Types of destinations */
    enum Where {
      /** Parameter is for the RCU */
      kRcu, 
      /** Parameter is for the BC */
      kBc, 
      /** Parameter is for the ALTRO */
      kAltro, 
      /** Invalid destination */ 
      kInvalid
    };

    /** User constructor 
	@param name   Name of this parameter 
	@param dest   Destination 
	@param isBlob Whether it's a blob parameterd parameter 
	@param mask   Bit mask used for comparisons */
    Parameter(const std::string& name, 
	      Where              dest,
	      bool               isBlob, 
	      unsigned int       mask) 
      : fName(name), 
	fDestination(dest), 
	fIsBlob(isBlob),
	fMask(mask)
    {}
    /** Destructor */
    virtual ~Parameter() {}
    /** Type of List of parameters */
    typedef std::list<Parameter*> List;

    /** Print to standard out */ 
    virtual void Print(std::ostream& o=std::cout) const;
    /** Insert this object into the database 
	@param s Server to contact */
    virtual bool Insert(RcuDb::Server& s);
    /** Create a table for these objects 
	@param s Server to create the table in */
    static  bool Create(RcuDb::Server& s);
    /** Delete table for these objects 
	@param s Server to delete the table from */
    static  bool Drop(RcuDb::Server& s);
    /** Query the database for objects of this type 
	@param s Server to query
	@param l Return list 
	@param cond Optional condition
	@return List of objects of this type matching @a cond */
    static  bool Select(List& l, RcuDb::Server& s, const RcuDb::Sql& cond);
    /** Query the database for objects of this type 
	@param s Server to query
	@param l Return list 
	@param w Which destination.
	@param name Name
	@return List of objects of this type matching @a cond */
    static  bool Select(List& l, RcuDb::Server& s, Where w, 
			const std::string& name=std::string());

    /** @return Name of this parameter */
    const std::string& Name() const { return fName; }
    /** @return Destination for this parameter */
    Where Destination() const { return fDestination; }
    /** @return Whether this parameter has blob parameter */
    bool IsBlob() const { return fIsBlob; }
    /** @return bit mask used for comparisons */ 
    unsigned int Mask() const { return fMask; }

    /** Table name */
    static const std::string fgName;
  protected:
    /** Construct from a row 
	@param row Row to construct from */
    Parameter(RcuDb::Row& row);
    /** Name of this parameter */ 
    std::string fName;
    /** Destination */ 
    Where fDestination;
    /** Whether this has BLOB value */
    bool fIsBlob;
    /** Bit mask used for comparisions  */ 
    unsigned int fMask;
  };
  /** Output operator of Parameter::Where values
      @param o Output stream
      @param w Where value 
      @return @a o after appending @a w */
  std::ostream& operator<<(std::ostream& o, const Parameter::Where w);
  /** Input operator of Parameter::Where values
      @param i Input stream
      @param w Where value 
      @return @a i after extracting @a w */
  std::istream& operator>>(std::istream& i, Parameter::Where& w);
}
#endif
//
// EOF
//

