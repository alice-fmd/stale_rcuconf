// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Altro configurator class
*/
#ifndef RCUCONF_ALTRO_H
#define RCUCONF_ALTRO_H
#ifndef RCUCONF_COMPONENT_H
# include <rcuconf/Component.h>
#endif

namespace Rcuxx
{
  class Altro;
  class AltroRegister;
  class AltroCommand;
}

namespace RcuConf
{
  /** @class Altro 
      @brief Component configurator for the ALTRO. 
      @ingroup Components 
   */
  class Altro : public BusComponent 
  {
  public: 
    /** Constructor 
	@param altro Reference to low-level ALTRO interface */
    Altro(Rcuxx::Altro& altro) : fAltro(altro) {}
    /** Destructor */
    virtual ~Altro() {}

    /** Create entries for all parameters in the data base
	@param server Server to contact. 
	@return @c true on success, @c false otherwise */
    static bool Create(RcuDb::Server& server);
  protected:
    /** Map a parameter name to a register */ 
    Rcuxx::AltroRegister* Name2Register(const std::string& name);
    /** Map a parameter name to a command */ 
    Rcuxx::AltroCommand* Name2Command(const std::string& name);

    /** Reference to low-level interface */
    Rcuxx::Altro& fAltro;
  };
}
#endif
//
// EOF
// 
