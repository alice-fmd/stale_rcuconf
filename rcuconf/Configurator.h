// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Configurator.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Configurator class
*/
#ifndef RCUCONF_CONFIGURATOR_H
#define RCUCONF_CONFIGURATOR_H
#ifndef RCUCONF_PARAMETER_H
# include <rcuconf/Parameter.h>
#endif 
#ifndef __STRING__
# include <string>
#endif

namespace RcuDb 
{
  // Forward declaration 
  class Server;
}
  


/** @namespace RcuConf 
    @brief Namespace for all RCU configuration classes. 
 */
namespace RcuConf
{
  /** @defgroup Steer Steering classes */ 

  // Forward decls. 
  class Parameter;
  class Config;
  class Priority;
  class Value;
  class BlobValue;
  class SingleValue;
  class Address;
  class Component;
  class Rcu;
  class Bc;
  class Altro;
  
  /** @class Configurator 
      @brief Class to call Rcuxx::Rcu API to do configuration.  The
      actual values is obtained from the database connection. 
      @ingroup Steer 
   */
  class Configurator
  {
  public:
    /** Constructor 
	@param rcu    Connection to RCU 
	@param bc     Connection to BC  
	@param altro  Connection to ALTRO
	@param server Connection to database */
    Configurator(RcuDb::Server&   server,
		 Rcu&          rcu, 
		 Bc&           bc, 
		 Altro&        altro);
    /** Destructor */
    virtual ~Configurator() {}

    /** Write to the hardware via the RCU interface 
	@param tag    Configuration tag 
	@param x      X coordinate 
	@param y      Y coordinate 
	@param z      Z coordinate 
	@return number of words written to block, or 0. Negative
	values indicate errors. 
    */
    virtual int Write(int tag, 
		      int x, 
		      int y, 
		      int z);
    /** Get the last error string 
	@return The last error string */
    const std::string& ErrorString() const { return fErrorString; }
    /** @return Reference to the RCU component */
    Rcu& RCU() { return fRcu; }
    /** @return Reference to the BC component */
    Bc& BC() { return fBc; }
    /** @return Reference to the ALTRO component */
    Altro& ALTRO() { return fAltro; }
    /** Set debug mode 
	@param use If true, output debug messages */
    void SetDebug(bool use=true) { fDebug = use; }
  protected:
    /** Get the configuration */ 
    virtual bool GetCurrent(int tag, int x, int y, int z);
    /** Get the order */ 
    virtual bool GetOrder();
    /** Get the params */ 
    virtual bool GetParams();
    /** Write a parameter */ 
    virtual bool Write(Component& c, Parameter& p);
    /** Check that write worked
	@param ret Return value from write
	@param name Name of parameter
	@param addr Address used
	@return @c true on success, @c false otherwise  */
    virtual bool CheckWrite(int ret, const std::string& name, int addr);
    
    /** Connection to database */
    RcuDb::Server& fServer;
    /** Connection to RCU */
    Rcu& fRcu;
    /** Connection to BC  */
    Bc& fBc;
    /** Connection to ALTRO */
    Altro& fAltro;
    /** Last error message */ 
    std::string fErrorString;
    /** The current configuration */ 
    Config* fCurrent;
    /** The current order of parameters */
    Priority* fOrder;
    /** The current list of parameters */ 
    Parameter::List fParams;    
    /** Debug flag */
    bool fDebug;
  };
}
#endif
//
// EOF
//

  
