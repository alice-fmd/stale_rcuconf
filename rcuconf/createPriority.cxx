#include "config.h"
#include "Options.h"
#include "Priority.h"
#include "Parameter.h"
#include <rcudb/Server.h>
#include <rcudb/Sql.h>
#include <iostream>
#include <sstream>
#include <stdexcept>


//____________________________________________________________________
int 
main(int argc, char** argv)
{
  using namespace RcuConf;
  Option<bool>        hOpt('h', "help",    "Show this help", false,false);
  Option<bool>        VOpt('V', "version", "Show version number", false,false);
  Option<std::string> cOpt('c', "connection", "Database connection url", 
			   "mysql://config@localhost/RCU");
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(cOpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << "raw2root version " << VERSION << std::endl;
    return 0;
  }

  
  std::string con = cOpt;
  try {
    RcuDb::Server* server = RcuDb::Server::Connect(con);
    if (!server) 
      throw std::runtime_error("Failed to open connection to server!");
  

    Parameter::List params;
    if (!Parameter::Select(params, *server, "")) {
      std::stringstream s;
      s << "Failed to get parameters: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }

    std::vector<int> order;
    for (Parameter::List::iterator i = params.begin(); i != params.end(); ++i)
      order.push_back((*i)->Id());
    Priority p("default", order);
    
    if (!p.Insert(*server)) {
      std::stringstream s;
      s << "Failed to get parameters: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }

    Priority::List orders;
    if (!Priority::Select(orders, *server, "")) {
      std::stringstream s;
      s << "Failed to get priorities: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }
    for (Priority::List::iterator i = orders.begin(); i != orders.end(); ++i) 
      (*i)->Print();
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}

      


