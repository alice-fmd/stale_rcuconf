#include "config.h"
#include "Options.h"
#include "Address.h"
#include <rcudb/Server.h>
#include <rcudb/Sql.h>
#include <iostream>
#include <sstream>
#include <stdexcept>


//____________________________________________________________________
int 
main(int argc, char** argv)
{
  using namespace RcuConf;
  Option<bool>        hOpt('h', "help",    "Show this help", false,false);
  Option<bool>        VOpt('V', "version", "Show version number", false,false);
  Option<bool>        dOpt('d', "dump",    "Dump to standard out", false,false);
  Option<std::string> cOpt('c', "connection", "Database connection url", 
			   "mysql://config@localhost/RCU");
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(cOpt);
  cl.Add(dOpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << "raw2root version " << VERSION << std::endl;
    return 0;
  }

  
  RcuConf::Table::Mode(dOpt.IsSet());
  std::string con = cOpt;
  try {
    RcuDb::Server* server = 0;
    if (!dOpt.IsSet()) {
      server = RcuDb::Server::Connect(con);
      if (!server) 
	throw std::runtime_error("Failed to open connection to server!");
    }

    // This is for the FMD!
    unsigned int boards[]    = { 0, 0x1, 0x10, 0x11 };
    unsigned int nchips      = 3;
    
    for (unsigned iboard = 0; iboard < 4; iboard++) {
      unsigned int board = boards[iboard];
      for (unsigned int chip = 0; chip < nchips; chip++) {
	unsigned int nchannels = (chip == 1 ? 8 : 16);
	for (unsigned int channel = 0; channel < nchannels; channel++) {
	  Address a(board, chip, channel);
	  if (!a.Insert(*server)) {
	    std::stringstream s;
	    s << "Failed to insert address: " << server->ErrorString();
	    throw std::runtime_error(s.str());
	  }
	}
      }
    }
    
    Address::List addrs;
    if (!Address::Select(addrs, *server, "")) {
      std::stringstream s;
      s << "Failed to get addresss: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }    
    for (Address::List::iterator i = addrs.begin(); i != addrs.end(); ++i)
      (*i)->Print();
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}

      


