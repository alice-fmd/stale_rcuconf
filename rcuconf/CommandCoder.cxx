// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT CommandCoder interface 
//
/** @file    CommandCoder.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Implementation of CommandCoder interface 
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <rcuxx/Rcu.h>
#include <rcuxx/Bc.h>
#include <rcuxx/Fmd.h>
#include <rcuxx/Altro.h>
#include "CommandCoder.h"
#include "Rcu.h"
#include "Bc.h"
#include "Fmd.h"
#include "Altro.h"
#include "Configurator.h"
#include <rcudb/Server.h>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <sstream>
#ifndef DB_URL
# error Database URL not defined 
#endif
#ifndef RCUXX_URL
/** URL of the Rcu++ backend to use */
# define RCUXX_URL "coder:"
#endif

struct to_lower
{
  char operator()(char c) const
  {
    return std::tolower(c);
  }
};

namespace 
{
  /** Configuration file */ 
  std::string fConfigFile(CONFIG_FILE);
  // int len;
}

//____________________________________________________________________
void RcuConf::setConfigFile(const std::string& file)
{
  fConfigFile = file;
}

//____________________________________________________________________
CommandCoderBase* CommandCoderBase::instance = new RcuConf::CommandCoder;

//____________________________________________________________________
RcuConf::CommandCoder::CommandCoder()
  : fServer(0), 
    fRcuxx(0), 
    fBcxx(0), 
    fAltroxx(0), 
    fRcu(0), 
    fBc(0), 
    fAltro(0),
    fConfigurator(0), 
    fRet(0),
    fVerbose(false), 
    fDebug(false),
    fInit(false),
    fDetector("tpc")
{
  // Note, that this method throws on error 
  std::cout << "Constructing CommandCoder" << std::endl;
  fDbUrl      = DB_URL;
  fRcuUrl     = RCUXX_URL;
  fVerbose    = false;  
}

//____________________________________________________________________
void
RcuConf::CommandCoder::init() 
{
  if (fInit) return;
  
  // Read the configuration file 
  readConfig();

  // Connect to DB
  std::cout << "Opening connection to DB on " << fDbUrl << std::endl;
  fServer = RcuDb::Server::Connect(fDbUrl);
  if (!fServer) throw std::runtime_error("Failed to open connection to db!");
      
  // Connect to FEE
  std::cout << "Opening connection to RCU on " << fRcuUrl << std::endl;
  fRcuxx = Rcuxx::Rcu::Open(fRcuUrl.c_str(), false, fDebug);
  if (!fRcuxx) throw std::runtime_error("Failed to open connection to RCU");
      
  std::transform(fDetector.begin(), fDetector.end(), 
		 fDetector.begin(), to_lower());
  bool is_fmd = (fDetector == "fmd");
  if      (is_fmd) fBcxx = new Rcuxx::Fmd(*fRcuxx);
  else             fBcxx = new Rcuxx::Bc(*fRcuxx);
  fAltroxx               = new Rcuxx::Altro(*fRcuxx);
      
  // Make components 
  fRcu               = new RcuConf::Rcu(*fRcuxx);
  if    (is_fmd) fBc = new RcuConf::Fmd(*fBcxx);
  else           fBc = new RcuConf::Bc(*fBcxx);
  fAltro             = new RcuConf::Altro(*fAltroxx);
      
  // Make command coder
  fConfigurator = new Configurator(*fServer, *fRcu, *fBc, *fAltro);
  fConfigurator->SetDebug(fDebug);
  fInit = true;
}

//____________________________________________________________________
bool
RcuConf::CommandCoder::readConfig()
{
  std::cout << "Reading configuration file " << fConfigFile << std::endl;
  std::ifstream config(fConfigFile.c_str());
  if (!config) return false;
  
  unsigned int line = 0;
  std::string  tmp;
  while (true) {
    char c = config.peek();
    while (true) {
      if (c == ' ' || c == '\t') { // || c == '\n') {
	config.get();
	if (c == '\n') line++;
	continue;
      }
      c = config.peek();
      break;
    }
    if (c == '#') {
      std::getline(config, tmp);
      line++;
      continue;
    }
    std::string key, val;
    config >> key >> val;
    std::transform(key.begin(), key.end(), key.begin(), to_lower());

    if (key[0] != '#') 
      std::cout << "Line # " << line << " Read key=" << key 
		<< " value=" << val << std::endl;
    if      (key == "db:")       fDbUrl    = val;
    else if (key == "rcu:")      fRcuUrl   = val;
    else if (key == "detector:") fDetector = val;
    else if (key == "verbose:") {
      std::transform(val.begin(), val.end(), val.begin(), to_lower());
      fVerbose = (val == "yes" || val == "true" || val == "on" || val == "1");
    }
    else if (key == "debug:") {
      std::transform(val.begin(), val.end(), val.begin(), to_lower());
      fDebug = (val == "yes" || val == "true" || val == "on" || val == "1");
    }
    std::getline(config, tmp);
    line++;
    if (config.eof()) break;
  }

  config.close();
  return true;
}


//____________________________________________________________________
int
RcuConf::CommandCoder::createDataBlock(char *target, int tag)
{
  // Initialize if not already done
  init();
  
  // Clear all errors. 
  fErrors.clear();
  if (!fConfigurator) return 0;
  
  // Try to find the number part 
  if (fVerbose) 
    std::cout << "Target: " << target << " tag: " << tag << std::endl;
  std::string s(target);
  std::string::size_type us = s.find('_');
  if (us == std::string::npos) {
    std::stringstream str;
    str << "Didn't find separating '_' in \"" << s << "\"";
    fErrors.push_back(str.str());
    return -1;
  }

  // Extract the coordinates. 
  std::string nums = s.substr(us+1, s.size() - us);
  std::stringstream str(nums);
  int x, y, z;
  char us1, us2;
  str >> x >> us1 >> y >> us2 >> z;
  if (str.bad() || us1 != '_' || us2 != '_') {
    std::stringstream str;
    str << "Badly formated target \"" << s << "\"";
    fErrors.push_back(str.str());
    return -1;
  }
  if (fVerbose)
    std::cout << "X=" << x << " Y=" << y << " Z=" << z << std::endl;
  
  // Let the configurator do it's thing. 
  fRet = fConfigurator->Write(tag, x, y, z);
  if (fRet < 0) fErrors.push_back(fConfigurator->ErrorString());

  // Return number or words, or negative error code 
  if (fVerbose) std::cout << "Made block of size: " << fRet << std::endl;
  return fRet;
}

//____________________________________________________________________
long int* 
RcuConf::CommandCoder::getDataBlock()
{
  if (!fConfigurator) return 0;
  long int* ret = fConfigurator->RCU().GetBlock();
  if (fVerbose) std::cout << "Getting the data block: " << ret << std::endl;

  if (fDebug) {
    std::cout << std::hex << std::setfill('0');
    for (int i = 0; i < fRet; i++) 
      std::cout << "\t0x" << std::setw(8) << ret[i] << std::endl;
    std::cout << std::dec << std::setfill(' ');
  }
  
  return ret;
}


//____________________________________________________________________
//
// EOF
// 
