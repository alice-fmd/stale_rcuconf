// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Table.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of base class for tables
*/
#include "Table.h"
#include "Sequence.h"
#include <rcudb/Server.h>
#include <rcudb/Sql.h>
#include <rcudb/Row.h>
#include <iostream>

//_____________________________________________________________________
bool RcuConf::Table::fDump = false;

//_____________________________________________________________________
RcuConf::Table::Table(RcuDb::Row& row)
{
  row.Field(0, fId);
}


//_____________________________________________________________________
bool
RcuConf::Table::MakeId(RcuDb::Server& server) 
{
  // Get a unique ID from the sequence table 
  fId = RcuConf::Sequence::Increment(server);
  if (fId < 0) return false;
  return true;
}
//_____________________________________________________________________
RcuDb::Sql& 
RcuConf::Table::IdOut(RcuDb::Sql& sql) 
{
  if (fDump) sql << "@id,";
  else       sql << fId << ",";
  return sql;
}

//_____________________________________________________________________
bool 
RcuConf::Table::Dump(const RcuDb::Sql& sql)
{
  if (fDump) {
    std::cout << sql << ";" << std::endl;
    return false;
  }
  return true;
}

//_____________________________________________________________________
bool
RcuConf::Table::Drop(RcuDb::Server& server, const std::string& table) 
{
  RcuDb::Sql sql;
  sql << "DROP TABLE " << table;
  if (fDump) {
    std::cout << sql << ";" << std::endl;
    return true;
  }
  return server.Exec(sql);
}

//_____________________________________________________________________
//
// EOF
//


  

  
