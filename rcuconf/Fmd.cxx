// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Fmd.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Fmd configurator class
*/
#include "Fmd.h"
#include <rcuxx/Fmd.h>
#include <rcuxx/fmd/FmdAL_ANA_I.h>
#include <rcuxx/fmd/FmdAL_ANA_U.h>
#include <rcuxx/fmd/FmdAL_DIG_I.h>
#include <rcuxx/fmd/FmdAL_DIG_U.h>
#include <rcuxx/fmd/FmdCalIter.h>
#include <rcuxx/fmd/FmdClock.h>
#include <rcuxx/fmd/FmdCommand.h>
#include <rcuxx/fmd/FmdConstants.h>
#include <rcuxx/fmd/FmdFLASH_I.h>
#include <rcuxx/fmd/FmdGTL_U.h>
#include <rcuxx/fmd/FmdHoldWait.h>
#include <rcuxx/fmd/FmdL0Timeout.h>
#include <rcuxx/fmd/FmdL0Triggers.h>
#include <rcuxx/fmd/FmdL1Timeout.h>
#include <rcuxx/fmd/FmdL1Triggers.h>
#include <rcuxx/fmd/FmdPulser.h>
#include <rcuxx/fmd/FmdRange.h>
#include <rcuxx/fmd/FmdRegister.h>
#include <rcuxx/fmd/FmdSampleClock.h>
#include <rcuxx/fmd/FmdShapeBias.h>
#include <rcuxx/fmd/FmdShiftClock.h>
#include <rcuxx/fmd/FmdStatus.h>
#include <rcuxx/fmd/FmdT1.h>
#include <rcuxx/fmd/FmdT1SENS.h>
#include <rcuxx/fmd/FmdT2.h>
#include <rcuxx/fmd/FmdT2SENS.h>
#include <rcuxx/fmd/FmdT3.h>
#include <rcuxx/fmd/FmdT4.h>
#include <rcuxx/fmd/FmdTopBottom.h>
#include <rcuxx/fmd/FmdVA_REC_IM.h>
#include <rcuxx/fmd/FmdVA_REC_IP.h>
#include <rcuxx/fmd/FmdVA_REC_UM.h>
#include <rcuxx/fmd/FmdVA_REC_UP.h>
#include <rcuxx/fmd/FmdVA_SUP_IM.h>
#include <rcuxx/fmd/FmdVA_SUP_IP.h>
#include <rcuxx/fmd/FmdVA_SUP_UM.h>
#include <rcuxx/fmd/FmdVA_SUP_UP.h>
#include <rcuxx/fmd/FmdVFP.h>
#include <rcuxx/fmd/FmdVFS.h>
#include <rcudb/Server.h>

//____________________________________________________________________
RcuConf::Fmd::Fmd(Rcuxx::Bc& fmd)
  : Bc(fmd), fFmd(*(dynamic_cast<Rcuxx::Fmd*>(&fmd)))
{}

//____________________________________________________________________
Rcuxx::AltroRegister*
RcuConf::Fmd::Name2Register(const std::string& name) 
{
  Rcuxx::AltroRegister* reg = 0;
  if      (name == "TEMP_TH")		reg = 0; // ~FMD fBc.TEMP_TH();
  else if (name == "AV_TH")		reg = 0; // ~FMD fBc.AV_TH();
  else if (name == "AC_TH")		reg = 0; // ~FMD fBc.AC_TH();
  else if (name == "DV_TH")		reg = 0; // ~FMD fBc.DV_TH();
  else if (name == "DC_TH")		reg = 0; // ~FMD fBc.DC_TH();
  else if (name == "ShapeBias")		reg = fFmd.ShapeBias();
  else if (name == "VFP")		reg = fFmd.VFP();
  else if (name == "VFS")		reg = fFmd.VFS();
  else if (name == "Pulser")		reg = fFmd.Pulser();
  else if (name == "CalIter")		reg = fFmd.CalIter();
  else if (name == "ShiftClock")	reg = fFmd.ShiftClock();
  else if (name == "SampleClock")	reg = fFmd.SampleClock();
  else if (name == "HoldWait")		reg = fFmd.HoldWait();
  else if (name == "L0Timeout")		reg = fFmd.L0Timeout();
  else if (name == "L1Timeout")		reg = fFmd.L1Timeout();
  else if (name == "Range")		reg = fFmd.Range();
  else if (name == "L0Triggers")	reg = 0; // R/O fFmd.L0Triggers();
  else if (name == "Status")		reg = 0; // R/O fFmd.Status();
  else if (name == "T1")		reg = 0; // R/O fFmd.T1();
  else if (name == "T1_TH")		reg = fFmd.T1_TH();
  else if (name == "FLASH_I")		reg = 0; // R/O fFmd.FLASH_I();
  else if (name == "FLASH_I_TH")	reg = fFmd.FLASH_I_TH();
  else if (name == "AL_DIG_I")		reg = 0; // R/O fFmd.AL_DIG_I();
  else if (name == "AL_DIG_I_TH")	reg = fFmd.AL_DIG_I_TH();
  else if (name == "AL_ANA_I")		reg = 0; // R/O fFmd.AL_ANA_I();
  else if (name == "AL_ANA_I_TH")	reg = fFmd.AL_ANA_I_TH();
  else if (name == "VA_REC_IP")		reg = 0; // R/O fFmd.VA_REC_IP();
  else if (name == "VA_REC_IP_TH")	reg = fFmd.VA_REC_IP_TH();
  else if (name == "T2")		reg = 0; // R/O fFmd.T2();
  else if (name == "T2_TH")		reg = fFmd.T2_TH();
  else if (name == "VA_SUP_IP")		reg = 0; // R/O fFmd.VA_SUP_IP();
  else if (name == "VA_SUP_IP_TH")	reg = fFmd.VA_SUP_IP_TH();
  else if (name == "VA_REC_IM")		reg = 0; // R/O fFmd.VA_REC_IM();
  else if (name == "VA_REC_IM_TH")	reg = fFmd.VA_REC_IM_TH();
  else if (name == "VA_SUP_IM")		reg = 0; // R/O fFmd.VA_SUP_IM();
  else if (name == "VA_SUP_IM_TH")	reg = fFmd.VA_SUP_IM_TH();
  else if (name == "GTL_U")		reg = 0; // R/O fFmd.GTL_U();
  else if (name == "GTL_U_TH")		reg = fFmd.GTL_U_TH();
  else if (name == "T3")		reg = 0; // R/O fFmd.T3();
  else if (name == "T3_TH")		reg = fFmd.T3_TH();
  else if (name == "T1SENS")		reg = 0; // R/O fFmd.T1SENS();
  else if (name == "T1SENS_TH")		reg = fFmd.T1SENS_TH();
  else if (name == "T2SENS")		reg = 0; // R/O fFmd.T2SENS();
  else if (name == "T2SENS_TH")		reg = fFmd.T2SENS_TH();
  else if (name == "AL_DIG_U")		reg = 0; // R/O fFmd.AL_DIG_U();
  else if (name == "AL_DIG_U_TH")	reg = fFmd.AL_DIG_U_TH();
  else if (name == "AL_ANA_U")		reg = 0; // R/O fFmd.AL_ANA_U();
  else if (name == "AL_ANA_U_TH")	reg = fFmd.AL_ANA_U_TH();
  else if (name == "T4")		reg = 0; // R/O fFmd.T4();
  else if (name == "T4_TH")		reg = fFmd.T4_TH();
  else if (name == "VA_REC_UP")		reg = 0; // R/O fFmd.VA_REC_UP();
  else if (name == "VA_REC_UP_TH")	reg = fFmd.VA_REC_UP_TH();
  else if (name == "VA_SUP_UP")		reg = 0; // R/O fFmd.VA_SUP_UP();
  else if (name == "VA_SUP_UP_TH")	reg = fFmd.VA_SUP_UP_TH();
  else if (name == "VA_SUP_UM")		reg = 0; // R/O fFmd.VA_SUP_UM();
  else if (name == "VA_SUP_UM_TH")	reg = fFmd.VA_SUP_UM_TH();
  else if (name == "VA_REC_UM")		reg = 0; // R/O fFmd.VA_REC_UM();
  else if (name == "VA_REC_UM_TH")	reg = fFmd.VA_REC_UM_TH();
  else                          	reg = Bc::Name2Register(name);
  return reg;
}

//____________________________________________________________________
Rcuxx::AltroCommand*
RcuConf::Fmd::Name2Command(const std::string& name) 
{
  Rcuxx::AltroCommand* cmd = 0;
  if      (name == "CalibrationRun")	cmd = fFmd.CalibrationRun();
  else if (name == "ChangeDacs")	cmd = fFmd.ChangeDacs();
  else if (name == "FakeTrigger")	cmd = fFmd.FakeTrigger();
  else if (name == "PulserOff")		cmd = fFmd.PulserOff();
  else if (name == "PulserOn")		cmd = fFmd.PulserOn();
  else if (name == "SoftReset")		cmd = fFmd.SoftReset();
  else if (name == "TestOff")		cmd = fFmd.TestOff();
  else if (name == "TestOn")		cmd = fFmd.TestOn();
  else                          	cmd = Bc::Name2Command(name);
  return cmd;
}

//____________________________________________________________________
bool
RcuConf::Fmd::Create(RcuDb::Server& s) 
{
  // Everything is in the RCU 
  Parameter::Where w = Parameter::kBc;
  // Create throws an exception in case of errors
  try {
    /* === Registers === */
    // Component::Create(s, "L1CNT",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "L2CNT",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "SCLKCNT",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "DSTBCNT",	w, false, 0xffffffff); // Read-only
    Component::Create(s, "TSMWORD",	w, false, 0x1ff);
    Component::Create(s, "USRATIO",	w, false, 0xffff);
    Component::Create(s, "CSR0",	w, false, 0x7fff);
    // Component::Create(s, "CSR1",	w, false, 0xffffffff); // Read-only
    Component::Create(s, "CSR2",	w, false, 0xffff);
    Component::Create(s, "CSR3",	w, false, 0xffff);
    // Component::Create(s, "Version",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "TEMP",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "AV",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "AC",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "DV",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "DC",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "TEMP_TH",	w, false, 0xffffffff); // Invalid
    // Component::Create(s, "AV_TH",	w, false, 0xffffffff); // Invalid
    // Component::Create(s, "AC_TH",	w, false, 0xffffffff); // Invalid
    // Component::Create(s, "DV_TH",	w, false, 0xffffffff); // Invalid
    // Component::Create(s, "DC_TH",	w, false, 0xffffffff); // Invalid
    Component::Create(s, "ShapeBias",		w, false, 0xffff);
    Component::Create(s, "VFP",			w, false, 0xffff);
    Component::Create(s, "VFS",			w, false, 0xffff);
    Component::Create(s, "Pulser",		w, false, 0xffff);
    Component::Create(s, "CalIter",		w, false, 0xffff);
    Component::Create(s, "ShiftClock",		w, false, 0xffff);
    Component::Create(s, "SampleClock",		w, false, 0xffff);
    Component::Create(s, "HoldWait",		w, false, 0xffff);
    Component::Create(s, "L0Timeout",		w, false, 0xffff);
    Component::Create(s, "L1Timeout",		w, false, 0xffff);
    Component::Create(s, "Range",		w, false, 0xffff);
    // Component::Create(s, "L0Triggers",	w, false, 0xffff); R/O
    // Component::Create(s, "Status",		w, false, 0xffff); R/O
    // Component::Create(s, "T1",		w, false, 0xffff); R/O
    Component::Create(s, "T1_TH",		w, false, 0xffff);
    // Component::Create(s, "FLASH_I",		w, false, 0xffff); R/O
    Component::Create(s, "FLASH_I_TH",		w, false, 0xffff);
    // Component::Create(s, "AL_DIG_I",		w, false, 0xffff); R/O
    Component::Create(s, "AL_DIG_I_TH",		w, false, 0xffff);
    // Component::Create(s, "AL_ANA_I",		w, false, 0xffff); R/O
    Component::Create(s, "AL_ANA_I_TH",		w, false, 0xffff);
    // Component::Create(s, "VA_REC_IP",	w, false, 0xffff); R/O
    Component::Create(s, "VA_REC_IP_TH",	w, false, 0xffff);
    // Component::Create(s, "T2",		w, false, 0xffff); R/O
    Component::Create(s, "T2_TH",		w, false, 0xffff);
    // Component::Create(s, "VA_SUP_IP",	w, false, 0xffff); R/O
    Component::Create(s, "VA_SUP_IP_TH",	w, false, 0xffff);
    // Component::Create(s, "VA_REC_IM",	w, false, 0xffff); R/O
    Component::Create(s, "VA_REC_IM_TH",	w, false, 0xffff);
    // Component::Create(s, "VA_SUP_IM",	w, false, 0xffff); R/O
    Component::Create(s, "VA_SUP_IM_TH",	w, false, 0xffff);
    // Component::Create(s, "GTL_U",		w, false, 0xffff); R/O
    Component::Create(s, "GTL_U_TH",		w, false, 0xffff);
    // Component::Create(s, "T3",		w, false, 0xffff); R/O
    Component::Create(s, "T3_TH",		w, false, 0xffff);
    // Component::Create(s, "T1SENS",		w, false, 0xffff); R/O
    Component::Create(s, "T1SENS_TH",		w, false, 0xffff);
    // Component::Create(s, "T2SENS",		w, false, 0xffff); R/O
    Component::Create(s, "T2SENS_TH",		w, false, 0xffff);
    // Component::Create(s, "AL_DIG_U",		w, false, 0xffff); R/O
    Component::Create(s, "AL_DIG_U_TH",		w, false, 0xffff);
    // Component::Create(s, "AL_ANA_U",		w, false, 0xffff); R/O
    Component::Create(s, "AL_ANA_U_TH",		w, false, 0xffff);
    // Component::Create(s, "T4",		w, false, 0xffff); R/O
    Component::Create(s, "T4_TH",		w, false, 0xffff);
    // Component::Create(s, "VA_REC_UP",	w, false, 0xffff); R/O
    Component::Create(s, "VA_REC_UP_TH",	w, false, 0xffff);
    // Component::Create(s, "VA_SUP_UP",	w, false, 0xffff); R/O
    Component::Create(s, "VA_SUP_UP_TH",	w, false, 0xffff);
    // Component::Create(s, "VA_SUP_UM",	w, false, 0xffff); R/O
    Component::Create(s, "VA_SUP_UM_TH",	w, false, 0xffff);
    // Component::Create(s, "VA_REC_UM",	w, false, 0xffff); R/O
    Component::Create(s, "VA_REC_UM_TH",	w, false, 0xffff);
    /* === Commands === */
    Component::Create(s, "ACQRDO",		w, false, 0x0);
    Component::Create(s, "ALRST",		w, false, 0x0);
    Component::Create(s, "BCRST",		w, false, 0x0);
    Component::Create(s, "CNTCLR",		w, false, 0x0);
    Component::Create(s, "CNTLAT",		w, false, 0x0);
    Component::Create(s, "CSR1CLR",		w, false, 0x0);
    Component::Create(s, "EVLRDO",		w, false, 0x0);
    Component::Create(s, "SCEVL",		w, false, 0x0);
    Component::Create(s, "STCNV",		w, false, 0x0);
    Component::Create(s, "STTSM",		w, false, 0x0);
    Component::Create(s, "CalibrationRun",	w, false, 0xffff);
    Component::Create(s, "ChangeDacs",		w, false, 0xffff);
    Component::Create(s, "FakeTrigger",		w, false, 0xffff);
    Component::Create(s, "PulserOff",		w, false, 0xffff);
    Component::Create(s, "PulserOn",		w, false, 0xffff);
    Component::Create(s, "SoftReset",		w, false, 0xffff);
    Component::Create(s, "TestOff",		w, false, 0xffff);
    Component::Create(s, "TestOn",		w, false, 0xffff);
  }
  catch (bool& e) {
    return e;
  }
  return true;
}


//____________________________________________________________________
//
// EOF
//
