// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Address.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Address table class
*/
#include "Address.h"
#include "Value.h"
#include "Sequence.h"
#include <rcudb/Server.h>
#include <rcudb/Result.h>
#include <rcudb/Row.h>
#include <rcudb/Sql.h>
#include <iostream>

namespace 
{
  const std::string fgName("Address");
}

//_____________________________________________________________________
RcuConf::Address::Address(RcuDb::Row& row) 
  : Table(row)
{
  row.Field(1, fAddress);
}

//_____________________________________________________________________
void
RcuConf::Address::Print(std::ostream& o) const
{
  o << "Address: id="     << fId 
    << "\taddress="       << fAddress << std::endl;
}

//_____________________________________________________________________
bool
RcuConf::Address::Insert(RcuDb::Server& server) 
{
  // Get previous versions. 
  RcuDb::Sql sql1;
  sql1 << "address=" << fAddress;
  List vers;
  if (!Select(vers, server, sql1)) return false;
  // We can have only one instance of an address
  if (vers.size() > 0) {
    Address* a = *(vers.begin());
    fId        = a->fId;
    return true;
  }

  // Get a unique ID from the sequence table 
  if (!MakeId(server)) return false;

  // And then do the insert. 
  RcuDb::Sql sql2;
  sql2 << "INSERT INTO " << fgName << " VALUES(";
  IdOut(sql2) << fAddress << ")";
  if (!Dump(sql2)) return true;
  return server.Exec(sql2);
}


//_____________________________________________________________________
bool
RcuConf::Address::Create(RcuDb::Server& server) 
{
  RcuDb::Sql sql;
  sql << "CREATE TABLE " << fgName << " ("
      << "  id          INT KEY, " 
      << "  address     INT NOT NULL, " 
      << "  INDEX(address))";
  if (!Dump(sql)) return true;
  return server.Exec(sql);
}

//_____________________________________________________________________
bool
RcuConf::Address::Drop(RcuDb::Server& server) 
{
  return Table::Drop(server, fgName);
}

//_____________________________________________________________________
bool
RcuConf::Address::Select(List& l, RcuDb::Server& server, const RcuDb::Sql& cond) 
{
  // Make the query 
  RcuDb::Sql     sql;
  sql << "SELECT * FROM " << fgName 
      << (cond.Text().empty() ? "" : " WHERE ") 
      << (cond.Text().empty() ? "" : cond);
  if (!Dump(sql)) return true;
  
  // Execute query, and check error status
  RcuDb::Result* res = server.Query(sql);
  if (server.IsError()) return false;

  // Make the result table if any 
  if (!res) return true;
  RcuDb::Row* row = 0;
  while ((row = res->Next())) l.push_back(new Address(*row));

  // delete result, and return OK.
  delete res;
  return true;  
}

//_____________________________________________________________________
bool
RcuConf::Address::Select(List& l, RcuDb::Server& server, const Value& v)
{
  RcuDb::Sql sql;
  sql << "id=" << v.AddressId();
  return Select(l, server, sql);
}

//_____________________________________________________________________
//
// EOF
//


  

  
