#include "config.h"
#include "Options.h"
#include "Priority.h"
#include "Parameter.h"
#include <rcudb/Server.h>
#include <rcudb/Sql.h>
#include <iostream>
#include <sstream>
#include <stdexcept>

//____________________________________________________________________
template <typename T>
void createTable(RcuDb::Server& s, const std::string& name) 
{
  if (T::Create(s)) return;
  
  std::stringstream errstr;
  errstr << "Failed to create " << name << " table: " 
	 << s.ErrorString();
  throw std::runtime_error(errstr.str());
}


//____________________________________________________________________
int 
main(int argc, char** argv)
{
  using namespace RcuConf;
  Option<bool>        hOpt('h', "help",    "Show this help", false,false);
  Option<bool>        VOpt('V', "version", "Show version number", false,false);
  Option<std::string> cOpt('c', "connection", "Database connection url", 
			   "mysql://config@localhost/RCU");
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(cOpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << "raw2root version " << VERSION << std::endl;
    return 0;
  }

  
  std::string con = cOpt;
  try {
    DB::Server* server = DB::Server::Connect(con);
    if (!server) 
      throw std::runtime_error("Failed to open connection to server!");
  

    Parameter::List params;
    if (!Parameter::Select(params, *server)) {
      std::stringstream s;
      s << "Failed to get parameters: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }

    std::vector<int> order;
    for (Parameter::List::iterator i = params.begin(); i != params.end(); ++i){
      (*i)->Print();
      order.push_back((*i)->Id());
    }
    Priority p("default", params);
    
    if (!p.Insert(*server)) {
      std::stringstream s;
      s << "Failed to get parameters: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }
    DB::Result* res = server->Query("SELECT * FROM Priority");
    if (!res) {
      std::stringstream s;
      s << "Failed to get priorities: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }
    res->Print();
    
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}

      


