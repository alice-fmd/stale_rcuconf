// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Value.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Value table class
*/
#include "Value.h"
#include "SingleValue.h"
#include "BlobValue.h"
#include "Sequence.h"
#include <rcudb/Server.h>
#include <rcudb/Result.h>
#include <rcudb/Sql.h>
#include <rcudb/Row.h>
#include <iostream>

//_____________________________________________________________________
RcuConf::Value::Value(RcuDb::Row& row) 
  : Table(row)
{
  row.Field(1, fConfigId);
  row.Field(2, fParamId);
  row.Field(3, fAddressId);
  row.Field(4, fVersion);
}

//_____________________________________________________________________
void
RcuConf::Value::Print(std::ostream& o) const
{
  o << "Value: id="     << fId 
    << "\tconfig="      << fConfigId
    << "\tparam="	<< fParamId 
    << "\taddress="	<< fAddressId 
    << "\tversion="	<< fVersion << std::flush;
}

//_____________________________________________________________________
bool
RcuConf::Value::Select(List& r, RcuDb::Server& server, 
		       const std::string& table, const RcuDb::Sql& cond)
{
  RcuDb::Result* res = 0;
  bool ret = Select(res, server, table, cond);
  if (fDump) return true;
  if (!ret) return ret;
  
  // Make the result table if any 
  if (!res) return true;

  // Check if this is a blob table
  bool blob = (table == BlobValue::fgName);

  RcuDb::Row* row = 0;
  while ((row = res->Next())) {
    Value*    v = 0;
    if (blob) v = new BlobValue(*row);
    else      v = new SingleValue(*row);
    r.push_back(v);
  }
  

  // delete result, and return OK.
  delete res;
  return true;  
}

//_____________________________________________________________________
bool
RcuConf::Value::Select(List& r, RcuDb::Server& s, 
		       const std::string& table, int config, int param, 
		       int addr)
{
  RcuDb::Result* res = 0;
  bool ret = Select(res, s, table, config, param, addr);
  if (fDump) return true;
  if (!ret) return ret;
  
  // Make the result table if any 
  if (!res) return true;

  // Check if this is a blob table
  bool blob = (table == BlobValue::fgName);

  RcuDb::Row* row = 0;
  while ((row = res->Next())) {
    Value* v = 0;
    if (blob) v = new BlobValue(*row);
    else      v = new SingleValue(*row);
    r.push_back(v);
  }
  
  // delete result, and return OK.
  delete res;
  return true;  
}

  
//_____________________________________________________________________
bool
RcuConf::Value::Select(RcuDb::Result*& r, RcuDb::Server& server, 
		       const std::string& table, const RcuDb::Sql& cond)
{
  // Set result to point to nothing. 
  r = 0;

  // Make query 
  RcuDb::Sql     sql;
  sql << "SELECT * FROM " << table
      << (cond.Text().empty() ? "" : " WHERE ") 
      << (cond.Text().empty() ? "" : cond);
  if (!Dump(sql)) return true;

  // Execute query, and check error status
  r = server.Query(sql);
  if (server.IsError()) { 
    if (r) {
      delete r;
      r = 0;
    }
    return false;
  }
  return true;
}

//_____________________________________________________________________
bool
RcuConf::Value::Select(RcuDb::Result*& r, RcuDb::Server& s, 
		       const std::string& table, int config, int param, 
		       int addr)
{
  RcuDb::Sql cond;
  cond << "configid=" << config << " AND "
       << "paramid="  << param  << " AND " 
       << "addressid";
  if      (addr <  0) cond << "<=0";
  else if (addr == 0) cond << ">0";
  else if (addr >  0) cond << "=" << addr;
  cond << " ORDER BY " << (addr <= 0 ? "" : "addressid ASC, ") 
       << "version DESC";
  // std::cout << "Select condition: " << cond.Text() << std::endl;
  return Select(r, s, table, cond);
}


//_____________________________________________________________________
RcuDb::Sql&
RcuConf::Value::ValueInsert(RcuDb::Sql& sql) 
{
  // Get previous versions. 
  IdOut(sql) << fConfigId << "," << fParamId << "," 
	     << fAddressId << "," << fVersion << ",";
  return sql;
}


//_____________________________________________________________________
RcuDb::Sql&
RcuConf::Value::ValueCreate(RcuDb::Sql& sql) 
{
  sql << " id         INT KEY, "
      << " configid   INT NOT NULL, "
      << " paramid    INT NOT NULL, "
      << " addressid  INT NOT NULL, "
      << " version    INT NOT NULL, ";
  return sql;
}


//_____________________________________________________________________
//
// EOF
//


  

  
