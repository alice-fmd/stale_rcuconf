#include "config.h"
#include "Options.h"
#include "Rcu.h"
#include "Altro.h"
#include "Bc.h"
#include "Fmd.h"
#include <rcudb/Server.h>
#include <rcudb/Sql.h>
#include <iostream>
#include <sstream>
#include <stdexcept>

//____________________________________________________________________
template <typename T>
void
createComponent(RcuDb::Server& s, const std::string& name) 
{
  if (T::Create(s)) return;
  std::stringstream errstr;
  errstr << "Failed to create parameters for " << name << ": "
	 << s.ErrorString() << std::endl;
  throw std::runtime_error(errstr.str());
}

//____________________________________________________________________  
int 
main(int argc, char** argv)
{
  using namespace RcuConf;
  Option<bool>        hOpt('h', "help",    "Show this help", false,false);
  Option<bool>        VOpt('V', "version", "Show version number", false,false);
  Option<bool>        fOpt('f', "fmd",     "Use FMD", false,false);
  Option<std::string> cOpt('c', "connection", "Database connection url", 
			   "mysql://config@localhost/RCU");
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(fOpt);
  cl.Add(cOpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << "raw2root version " << VERSION << std::endl;
    return 0;
  }

  try {
    std::string con = cOpt;
    RcuDb::Server* server = RcuDb::Server::Connect(con);
    if (!server) 
      throw std::runtime_error("Failed to open connection to server!");

    createComponent<Rcu>(*server, "RCU");
    if (fOpt.IsSet()) 
      createComponent<Fmd>(*server, "FMD");
    else
      createComponent<Bc>(*server, "BC");
    createComponent<Altro>(*server, "ALTRO");

    Parameter::List params;
    if (!Parameter::Select(params, *server, "")) {
      std::stringstream s;
      s << "Failed to get parameters: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }

    for (Parameter::List::iterator i = params.begin(); i != params.end(); ++i)
      (*i)->Print();
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}

      


