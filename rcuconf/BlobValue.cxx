// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    BlobValue.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of BlobValue table class
*/
#include "BlobValue.h"
#include "Parameter.h"
#include "Config.h"
#include "Sequence.h"
#include <rcudb/Server.h>
#include <rcudb/Result.h>
#include <rcudb/Row.h>
#include <rcudb/Sql.h>
#include <iostream>

const std::string RcuConf::BlobValue::fgName("BlobValue");

//_____________________________________________________________________
RcuConf::BlobValue::BlobValue(RcuDb::Row& row) 
  : Value(row)
{
  row.Field(5, fValue);
}

//_____________________________________________________________________
void
RcuConf::BlobValue::Print(std::ostream& o) const
{
  Value::Print();
  o << "\tvalue="       << fValue << std::endl;
}

//_____________________________________________________________________
bool
RcuConf::BlobValue::Insert(RcuDb::Server& server) 
{
  // Get previous versions. 
  List vers;
  if (!Select(vers, server, fConfigId, fParamId,
	      (fAddressId <= 0 ? -1 : fAddressId))) return false;
  // We can have only one instance of an blobvalue
  fVersion = 0;
  if (vers.size() > 0) {
    BlobValue* v = *(vers.begin());
    fVersion     = v->fVersion +  1;
  }

  // Get a unique ID from the sequence table 
  if (!MakeId(server)) return false;

  // And then do the insert. 
  RcuDb::Sql sql2;
  sql2 << "INSERT INTO " << fgName << " VALUES(";
  ValueInsert(sql2) << "'" << fValue << "')";
  if (!Dump(sql2)) return true;
  return server.Exec(sql2);
}


//_____________________________________________________________________
bool
RcuConf::BlobValue::Create(RcuDb::Server& server) 
{
  RcuDb::Sql sql;
  sql << "CREATE TABLE " << fgName << " (";
  Value::ValueCreate(sql) << "  value     BLOB, " 
			  << "  INDEX(configid)," 
			  << "  INDEX(paramid), "
			  << "  INDEX(version))";
  if (!Dump(sql)) return true;
  return server.Exec(sql);
}

//_____________________________________________________________________
bool
RcuConf::BlobValue::Drop(RcuDb::Server& server) 
{
  return Table::Drop(server, fgName);
}

//_____________________________________________________________________
bool
RcuConf::BlobValue::Select(List& l, RcuDb::Server& server, const RcuDb::Sql& cond) 
{
  // Make the query 
  RcuDb::Result* res = 0;
  if (!Value::Select(res, server, fgName, cond)) return false;
  if (fDump) return true;
  
  // Make the result table if any 
  if (!res) return true;
  RcuDb::Row* row = 0;
  while ((row = res->Next())) l.push_back(new BlobValue(*row));

  // delete result, and return OK.
  delete res;
  return true;  
}

//_____________________________________________________________________
bool
RcuConf::BlobValue::Select(List& l, RcuDb::Server& server, 
			   const Config& c, const Parameter& p, int addr)
{
  // If parameter object p does not describe a blob, then fail. 
  if (!p.IsBlob()) return false;
  return Select(l, server, c.Id(), p.Id(), addr);
}

//_____________________________________________________________________
bool
RcuConf::BlobValue::Select(List& l, RcuDb::Server& server, 
			   int config, int param, int addr)
{
  RcuDb::Result* res = 0;
  if (!Value::Select(res, server, fgName, config, param, addr)) return false;
  if (fDump) return true;

  // Make the result table if any 
  if (!res) return true;
  RcuDb::Row* row = 0;
  while ((row = res->Next())) l.push_back(new BlobValue(*row));

  // delete result, and return OK.
  delete res;
  return true;  
}

//_____________________________________________________________________
//
// EOF
//


  

  
