// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Value.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Value table class
*/
#ifndef RCUCONF_VALUE_H
#define RCUCONF_VALUE_H
#ifndef RCUCONF_TABLE_H
# include <rcuconf/Table.h>
#endif
#ifndef __LIST__
# include <list>
#endif
#include <iostream>

namespace RcuDb 
{
  class Result;
  class Row;
  class Sql;
}

namespace RcuConf 
{
  
  /** @class Value
      @brief Values of parameters.  This is technically not really a
      table.  It's a base class for SingleValue and BlobValue tables. 
      @ingroup Tables
  */
  class Value : public Table 
  {
  public:
    /** User constructor 
	@param config  Reference to Config table 
	@param param   Reference to Parameter table
	@param address Reference to Address table */
    Value(int config, 
	  int param, 
	  int address) 
      : fConfigId(config), 
	fParamId(param), 
	fAddressId(address),
	fVersion(-1)
    {
      if (fAddressId > 0) std::cout << "Address=" << address << std::endl;
    }
    /** Destructor */ 
    virtual ~Value() { }

    /** Type of a list of values */ 
    typedef std::list<Value*> List;
    
    /** Print to standard out */ 
    virtual void Print(std::ostream& o=std::cout) const;

    /** @return Tag identifier */ 
    int ConfigId() const { return fConfigId; }
    /** @return X coordinate */
    int ParamId() const { return fParamId; }
    /** @return Y coordinate */
    int AddressId() const { return fAddressId; }
    /** @return Version number */
    int Version() const { return fVersion; }

    /** Select some rows from a table @a table and return result in @a r
	@param r On return, the result
	@param s The server to contact. 
	@param table The table to look in. 
	@param cond The optional condition to use. 
	@return @c true on success, @c false otherwise */
    static bool Select(List& r, RcuDb::Server& s, 
		       const std::string& table, const RcuDb::Sql& cond);
    
    /** Select some rows from a table @a table and return result in @a r
	@param r On return, the result.  The result is sorted by
	version number in descending order (most recent version
	first). 
	@param s The server to contact. 
	@param table The table to look in. 
	@param config Configuration identifier to match 
	@param param  Parameter identifier to match
	@param addr  Whether to only look for broadcast values (value
	@f$< 0@f$), non-broadcast values in general (value = 0), or a
	specific address (value @f$> 0@f$). 
	@return @c true on success, @c false otherwise */
    static bool Select(List& r, RcuDb::Server& s, 
		       const std::string& table, int config, int param,
		       int addr=-1);
  protected:
    /** Select some rows from a table @a table and return result in @a r
	@param r On return, the result
	@param s The server to contact. 
	@param table The table to look in. 
	@param cond The optional condition to use. 
	@return @c true on success, @c false otherwise */
    static bool Select(RcuDb::Result*& r, RcuDb::Server& s, 
		       const std::string& table, const RcuDb::Sql& cond);
    /** Select some rows from a table @a table and return result in @a r
	@param r On return, the result.  The result is sorted by
	version number in descending order (most recent version
	first). 
	@param s The server to contact. 
	@param table The table to look in. 
	@param config Configuration identifier to match 
	@param param  Parameter identifier to match
	@param addr  Whether to only look for broadcast values (value
	@f$< 0@f$), non-broadcast values in general (value = 0), or a
	specific address (value @f$> 0@f$). 
	@return @c true on success, @c false otherwise */
    static bool Select(RcuDb::Result*& r, RcuDb::Server& s, 
		       const std::string& table, int config, int param,
		       int addr=-1);

    /** The value part of an insert.
	@param sql SQL object to write to
	@return  @a sql after appending value specific stuff */
    RcuDb::Sql& ValueInsert(RcuDb::Sql& sql);
    
    /** Create Value part of a create statement. 
	@param sql Statement to append to 
	@return @a sql after appending */
    static RcuDb::Sql& ValueCreate(RcuDb::Sql& sql);

    /** Construct from a row 
	@param row Row to construct from */
    Value(RcuDb::Row& row);
    /** Reference to Value table */
    int fConfigId;
    /** Reference to Parameter table */
    int fParamId;
    /** Reference to address row. */
    int fAddressId;
    /** Version number.  Automatically updated. */
    int fVersion;
  };
}

#endif
//
// EOF
//

  

