#include "config.h"
#include "Options.h"
#include "Sequence.h"
#include "Config.h"
#include "Priority.h"
#include "Parameter.h"
#include "SingleValue.h"
#include "BlobValue.h"
#include "Address.h"
#include <rcudb/Server.h>
#include <rcudb/Result.h>
#include <rcudb/Sql.h>
#include <iostream>
#include <sstream>
#include <stdexcept>

//____________________________________________________________________
template <typename T>
void createTable(RcuDb::Server& s, const std::string& name) 
{
  if (T::Create(s)) return;
  
  std::stringstream errstr;
  errstr << "Failed to create " << name << " table: " 
	 << s.ErrorString();
  throw std::runtime_error(errstr.str());
}


//____________________________________________________________________
int 
main(int argc, char** argv)
{
  using namespace RcuConf;
  Option<bool>        hOpt('h', "help",    "Show this help", false,false);
  Option<bool>        VOpt('V', "version", "Show version number", false,false);
  Option<std::string> cOpt('c', "connection", "Database connection url", 
			   "mysql://config@localhost/RCU");
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(cOpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << "raw2root version " << VERSION << std::endl;
    return 0;
  }

  
  std::string con = cOpt;
  try {
    RcuDb::Server* server = RcuDb::Server::Connect(con);
    if (!server) 
      throw std::runtime_error("Failed to open connection to server!");
  
    if (!Sequence::Create(*server)) {
      std::stringstream s;
      s << "Failed to create Sequence table: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }
    createTable<Config>(*server,      "Config");
    createTable<Priority>(*server,       "Priority");
    createTable<Parameter>(*server,   "Parameter");
    createTable<SingleValue>(*server, "SingleValue");
    createTable<BlobValue>(*server,   "BlobValue");
    createTable<Address>(*server,     "Address");

    RcuDb::Result* res = server->Query("SHOW TABLES");
    if (!res) {
      std::stringstream s;
      s << "Failed to get priorities: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }
    res->Print();
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}

      


