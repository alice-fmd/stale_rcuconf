#include "config.h"
#include "Options.h"
#include "Config.h"
#include "Priority.h"
#include "Parameter.h"
#include "SingleValue.h"
#include "BlobValue.h"
#include "Address.h"
#include <rcudb/Server.h>
#include <rcudb/Sql.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <algorithm>

void
createValue(RcuDb::Server& s, 
	    int configId, int paramId, bool blob, int addrId)
{
  if (blob) {
    std::vector<unsigned int> array;
    for (size_t i = 0; i < 10; i++) array.push_back(i+1);
    RcuConf::BlobValue b(configId, paramId, addrId, array);
    if (addrId > 0) b.Print();
    if (b.Insert(s)) return;
  }
  else {
    RcuConf::SingleValue v(configId, paramId, addrId, paramId);
    if (addrId > 0) v.Print();
    if (v.Insert(s)) return;
  }
  std::stringstream errstr;
  errstr << "Failed to insert " << (blob ? "Blob" : "Single") 
	 << "Value for (" << configId << "," << paramId << ","
	 << addrId << "): " << s.ErrorString();
  throw std::runtime_error(errstr.str());
}


//____________________________________________________________________
int 
main(int argc, char** argv)
{
  using namespace RcuConf;
  Option<bool>        hOpt('h', "help",    "Show this help", false,false);
  Option<bool>        VOpt('V', "version", "Show version number", false,false);
  Option<unsigned>    tOpt('t', "tag",     "Tag to use", 0);
  Option<unsigned>    xOpt('x', "x",       "X coordinate", 0);
  Option<unsigned>    yOpt('y', "y",       "Y coordinate", 0);
  Option<unsigned>    zOpt('z', "z",       "Z coordinate", 0);
  Option<std::string> cOpt('c', "connection", "Database connection url", 
			   "mysql://config@localhost/RCU");
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(cOpt);
  cl.Add(tOpt);
  cl.Add(xOpt);
  cl.Add(yOpt);
  cl.Add(zOpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << "raw2root version " << VERSION << std::endl;
    return 0;
  }

  
  std::string con = cOpt;
  try {
    RcuDb::Server* server = RcuDb::Server::Connect(con);
    if (!server) 
      throw std::runtime_error("Failed to open connection to server!");
  

    // Get configuration 
    Config::List confs;
    if (!Config::Select(confs, *server, tOpt, xOpt, yOpt, zOpt)) {
      std::stringstream s;
      s << "Failed to get config (" << tOpt << "," << xOpt << "," 
	<< zOpt << "): " << server->ErrorString();
      throw std::runtime_error(s.str());
    }
    Config* config = *(confs.begin());
    
    // Get priority
    Priority::List priorities;
    if (!Priority::Select(priorities, *server, *config)) {
      std::stringstream s;
      s << "Failed to get priority for configuration " << config->Id() 
	<< ": " << server->ErrorString();
      throw std::runtime_error(s.str()); 
    }
    if (priorities.size() < 0)
      throw std::runtime_error("Too few matches on default priority");
    if (priorities.size() > 1) 
      throw std::runtime_error("Too many matches on default priority");
    Priority* priority = *(priorities.begin());
    
    // Get the parameters we need 
    std::vector<int> order;
    priority->Params(order);
    RcuDb::Sql parSel;
    for (std::vector<int>::iterator i = order.begin(); i != order.end(); ++i){
      if (i != order.begin()) parSel << " OR";
      parSel << " id=" << *i;
    }
    
    // Get the parameters 
    Parameter::List params;
    if (!Parameter::Select(params, *server, parSel)) {
      std::stringstream s;
      s << "Failed to get parameters: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }
    int nParam = params.size();
    
    // Get possible addresses 
    Address::List addrs;
    if (!Address::Select(addrs, *server, "")) {
      std::stringstream s;
      s << "Failed to get addresss: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }    
    int nAddress = addrs.size();

    // Loop over parameters
    for (Parameter::List::iterator i = params.begin(); i != params.end(); ++i){
      createValue(*server, config->Id(), (*i)->Id(), (*i)->IsBlob(), -1);
      if ((*i)->Destination() == Parameter::kRcu) continue;

      if ((*i)->Name() == "K1") {
	// Add this parameter a couple of times in broadcast 
	createValue(*server, config->Id(), (*i)->Id(), (*i)->IsBlob(), -1);
	createValue(*server, config->Id(), (*i)->Id(), (*i)->IsBlob(), -1);
      	// add a couple of times with an address. 
	unsigned a = (*(addrs.begin()))->Id();
	createValue(*server, config->Id(), (*i)->Id(), (*i)->IsBlob(), a);
	createValue(*server, config->Id(), (*i)->Id(), (*i)->IsBlob(), a);
	// Add this parameter a couple of times in broadcast 
	createValue(*server, config->Id(), (*i)->Id(), (*i)->IsBlob(), -1);
	createValue(*server, config->Id(), (*i)->Id(), (*i)->IsBlob(), -1);
      	// add a couple of times with an address. 
	createValue(*server, config->Id(), (*i)->Id(), (*i)->IsBlob(), a);
	createValue(*server, config->Id(), (*i)->Id(), (*i)->IsBlob(), a);
      }
      
      if (rand() % nParam > nParam / 3) continue;
      
      int m = std::min(nAddress, 4);
      Address::List::iterator iter = addrs.begin();
      while (m > 0 && iter != addrs.end()) {
	if ((rand() % nAddress) < m) {
	  std::cout << "Creating value with address id " 
		    << (*iter)->Id() << std::endl;
	  createValue(*server, config->Id(), (*i)->Id(), (*i)->IsBlob(), 
		      (*iter)->Id());
	  m--;
	}
	++iter;
      }      
    }

    // Print singles
    SingleValue::List singles;
    if (!SingleValue::Select(singles, *server, "")) {
      std::stringstream s;
      s << "Failed to get priorities: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }
    for (SingleValue::List::iterator i = singles.begin(); 
	 i != singles.end(); ++i) 
      (*i)->Print();

    // Print blobs 
    BlobValue::List blobs;
    if (!BlobValue::Select(blobs, *server, "")) {
      std::stringstream s;
      s << "Failed to get priorities: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }
    for (BlobValue::List::iterator i = blobs.begin(); i != blobs.end(); ++i) 
      (*i)->Print();

  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}

      


