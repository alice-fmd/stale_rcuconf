// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    CommandCoderBase.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:04 2006
    @brief   Declaration of InterCom Layer's interface to the command coder.
*/
#ifndef RCUCONF_COMMANDCODERBASE_H
#define RCUCONF_COMMANDCODERBASE_H
#ifndef __VECTOR__
# include <vector>
#endif
#ifndef __STRING__
# include <string>
#endif

/**
 * Base class for command coders 
 * @ingroup Steer 
 */
class CommandCoderBase
{
public:
  /** 
   * Create a data block 
   * @param target Where to send it 
   * @param tag The tag 
   * @return  Error code 
   */
  virtual int createDataBlock(char *target, int tag) = 0;
  /** 
   * Get the command block 
   * @return  data block 
   */
  virtual long int * getDataBlock() = 0;
  /** 
   * Get list of error strings 
   * @return  List of error  strings 
   */
  virtual std::vector<std::string> getError() = 0;
  /** 
   * Get singleton object 
   * @return The singleton object 
   */
  static CommandCoderBase* createInstance() {return instance;};

  /** Destructor */
  virtual ~CommandCoderBase() {}
private:
  /** The singleton object */
  static CommandCoderBase* instance;
};

#endif
//
// EOF
// 
