// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Bc.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Bc configurator class
*/
#include <cmath>
#include "Bc.h"
#include <rcuxx/Bc.h>
#include <rcuxx/bc/BcAC.h>
#include <rcuxx/bc/BcAC_TH.h>
#include <rcuxx/bc/BcAV.h>
#include <rcuxx/bc/BcAV_TH.h>
#include <rcuxx/bc/BcCommand.h>
#include <rcuxx/bc/BcCSR0.h>
#include <rcuxx/bc/BcCSR1.h>
#include <rcuxx/bc/BcCSR2.h>
#include <rcuxx/bc/BcCSR3.h>
#include <rcuxx/bc/BcDC.h>
#include <rcuxx/bc/BcDC_TH.h>
#include <rcuxx/bc/BcDSTBCNT.h>
#include <rcuxx/bc/BcDV.h>
#include <rcuxx/bc/BcDV_TH.h>
#include <rcuxx/bc/BcL1CNT.h>
#include <rcuxx/bc/BcL2CNT.h>
#include <rcuxx/bc/BcMonitored.h>
#include <rcuxx/bc/BcRegister.h>
#include <rcuxx/bc/BcSCLKCNT.h>
#include <rcuxx/bc/BcTEMP.h>
#include <rcuxx/bc/BcTEMP_TH.h>
#include <rcuxx/bc/BcThreshold.h>
#include <rcuxx/bc/BcTSMWORD.h>
#include <rcuxx/bc/BcUSRATIO.h>
#include <rcuxx/bc/BcVersion.h>
#include <rcudb/Row.h>
#include <rcudb/Sql.h>

//____________________________________________________________________
Rcuxx::AltroRegister*
RcuConf::Bc::Name2Register(const std::string& name) 
{
  Rcuxx::AltroRegister* reg = 0;
  if      (name == "L1CNT")	reg = 0; // Read-only fBc.L1CNT();
  else if (name == "L2CNT")	reg = 0; // Read-only fBc.L2CNT();
  else if (name == "SCLKCNT")	reg = 0; // Read-only fBc.SCLKCNT();
  else if (name == "DSTBCNT")	reg = 0; // Read-only fBc.DSTBCNT();
  else if (name == "TSMWORD")	reg = fBc.TSMWORD();
  else if (name == "USRATIO")	reg = fBc.USRATIO();
  else if (name == "CSR0")	reg = fBc.CSR0();
  else if (name == "CSR1")	reg = 0; // Read-only fBc.CSR1();
  else if (name == "CSR2")	reg = fBc.CSR2();
  else if (name == "CSR3")	reg = fBc.CSR3();
  else if (name == "Version")	reg = 0; // Read-only fBc.Version();
  else if (name == "TEMP")	reg = 0; // Read-only fBc.TEMP();
  else if (name == "AV")	reg = 0; // Read-only fBc.AV();
  else if (name == "AC")	reg = 0; // Read-only fBc.AC();
  else if (name == "DV")	reg = 0; // Read-only fBc.DV();
  else if (name == "DC")	reg = 0; // Read-only fBc.DC();
  else if (name == "TEMP_TH")	reg = fBc.TEMP_TH();
  else if (name == "AV_TH")	reg = fBc.AV_TH();
  else if (name == "AC_TH")	reg = fBc.AC_TH();
  else if (name == "DV_TH")	reg = fBc.DV_TH();
  else if (name == "DC_TH")	reg = fBc.DC_TH();
  return reg;
}

//____________________________________________________________________
Rcuxx::AltroCommand*
RcuConf::Bc::Name2Command(const std::string& name) 
{
  Rcuxx::AltroCommand* cmd = 0;
  if      (name == "ACQRDO")	cmd = fBc.ACQRDO();
  else if (name == "ALRST")	cmd = fBc.ALRST();
  else if (name == "BCRST")	cmd = fBc.BCRST();
  else if (name == "CNTCLR")	cmd = fBc.CNTCLR();
  else if (name == "CNTLAT")	cmd = fBc.CNTLAT();
  else if (name == "CSR1CLR")	cmd = fBc.CSR1CLR();
  else if (name == "EVLRDO")	cmd = fBc.EVLRDO();
  else if (name == "SCEVL")	cmd = fBc.SCEVL();
  else if (name == "STCNV")	cmd = fBc.STCNV();
  else if (name == "STTSM")	cmd = fBc.STTSM();
  return cmd;
}

//____________________________________________________________________
bool
RcuConf::Bc::Create(RcuDb::Server& s) 
{
  // Everything is in the RCU 
  Parameter::Where w = Parameter::kBc;
  // Create throws an exception in case of errors
  try {
    /* === Registers === */
    // Component::Create(s, "L1CNT",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "L2CNT",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "SCLKCNT",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "DSTBCNT",	w, false, 0xffffffff); // Read-only
    Component::Create(s, "TSMWORD",	w, false, 0x1ff);
    Component::Create(s, "USRATIO",	w, false, 0xffff);
    Component::Create(s, "CSR0",	w, false, 0x7ff);
    // Component::Create(s, "CSR1",	w, false, 0xffffffff); // Read-only
    Component::Create(s, "CSR2",	w, false, 0xffff);
    Component::Create(s, "CSR3",	w, false, 0xffff);
    // Component::Create(s, "Version",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "TEMP",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "AV",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "AC",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "DV",	w, false, 0xffffffff); // Read-only
    // Component::Create(s, "DC",	w, false, 0xffffffff); // Read-only
    Component::Create(s, "TEMP_TH",	w, false, 0x3ff);
    Component::Create(s, "AV_TH",	w, false, 0x3ff);
    Component::Create(s, "AC_TH",	w, false, 0x3ff);
    Component::Create(s, "DV_TH",	w, false, 0x3ff);
    Component::Create(s, "DC_TH",	w, false, 0x3ff);
    /* === Commands === */
    Component::Create(s, "ACQRDO",	w, false, 0x0);
    Component::Create(s, "ALRST",	w, false, 0x0);
    Component::Create(s, "BCRST",	w, false, 0x0);
    Component::Create(s, "CNTCLR",	w, false, 0x0);
    Component::Create(s, "CNTLAT",	w, false, 0x0);
    Component::Create(s, "CSR1CLR",	w, false, 0x0);
    Component::Create(s, "EVLRDO",	w, false, 0x0);
    Component::Create(s, "SCEVL",	w, false, 0x0);
    Component::Create(s, "STCNV",	w, false, 0x0);
    Component::Create(s, "STTSM",	w, false, 0x0);
  }
  catch (bool& e) {
    return e;
  }
  return true;
}


//____________________________________________________________________
//
// EOF
//
