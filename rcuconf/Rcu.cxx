// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Rcu.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Rcu configurator class
*/
#include "Rcu.h"
#include <rcuxx/Rcu.h>
#include <rcuxx/rcu/RcuRegister.h>
#include <rcuxx/rcu/RcuCommand.h>
#include <rcuxx/rcu/RcuMemory.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/rcu/RcuALTROCFG1.h>
#include <rcuxx/rcu/RcuALTROCFG2.h>
#include <rcuxx/rcu/RcuALTROIF.h>
#include <rcuxx/rcu/RcuBPVERS.h>
#include <rcuxx/rcu/RcuCHADD.h>
#include <rcuxx/rcu/RcuCommand.h>
#include <rcuxx/rcu/RcuCounter.h>
#include <rcuxx/rcu/RcuDMEM.h>
#include <rcuxx/rcu/RcuERRREG.h>
#include <rcuxx/rcu/RcuERRST.h>
#include <rcuxx/rcu/RcuEVWORD.h>
#include <rcuxx/rcu/RcuFECERR.h>
#include <rcuxx/rcu/RcuFMIREG.h>
#include <rcuxx/rcu/RcuFMOREG.h>
#include <rcuxx/rcu/RcuFWVERS.h>
#include <rcuxx/rcu/RcuHEADER.h>
#include <rcuxx/rcu/RcuIMEM.h>
#include <rcuxx/rcu/RcuINTMOD.h>
#include <rcuxx/rcu/RcuINTREG.h>
#include <rcuxx/rcu/RcuIRADD.h>
#include <rcuxx/rcu/RcuIRDAT.h>
#include <rcuxx/rcu/RcuL1Timeout.h>
#include <rcuxx/rcu/RcuLWADD.h>
#include <rcuxx/rcu/RcuMemory.h>
#include <rcuxx/rcu/RcuPMCFG.h>
#include <rcuxx/rcu/RcuPMEM.h>
#include <rcuxx/rcu/RcuRCUBUS.h>
#include <rcuxx/rcu/RcuRDOERR.h>
#include <rcuxx/rcu/RcuRDOFEC.h>
#include <rcuxx/rcu/RcuRDOMOD.h>
#include <rcuxx/rcu/RcuRegister.h>
#include <rcuxx/rcu/RcuRESREG.h>
#include <rcuxx/rcu/RcuRMEM.h>
#include <rcuxx/rcu/RcuSCADD.h>
#include <rcuxx/rcu/RcuSCDAT.h>
#include <rcuxx/rcu/RcuStatusEntry.h>
#include <rcuxx/rcu/RcuSTATUS.h>
#include <rcuxx/rcu/RcuTRCFG1.h>
#include <rcuxx/rcu/RcuTRCFG2.h>
#include <rcuxx/rcu/RcuTRCNT.h>
#include <rcuxx/rcu/RcuTRGCONF.h>
#include <rcudb/Server.h>
#include "SingleValue.h"
#include "BlobValue.h"

//____________________________________________________________________
Rcuxx::RcuRegister*
RcuConf::Rcu::Name2Register(const std::string& name) 
{
  Rcuxx::RcuRegister* reg = 0;
  // --- Error and status  ---
  if      (name == "ERRST")		reg = 0;	// fRcu.ERRST();
  else if (name == "TRCNT")		reg = 0;	// fRcu.TRCNT();
  else if (name == "RDOFEC")		reg = 0;	// fRcu.RDOFEC();
  else if (name == "CHADD")		reg = 0;	// fRcu.CHADD();
  else if (name == "FWVERS")		reg = 0;	// fRcu.FWVERS();
  else if (name == "BPVERS") 		reg = 0;	// fRcu.BPVERS();
  else if (name == "FECERRA") 		reg = 0;	// fRcu.FECERRA();
  else if (name == "FECERRB") 		reg = 0;	// fRcu.FECERRB();
  else if (name == "RDOERR")		reg = 0;	// fRcu.RDOERR();
  else if (name == "RCUBUS")		reg = 0;	// fRcu.RCUBUS();
       
    
  // --- U2F Specific registers ---
  else if (name == "LWADD")		reg = fRcu.LWADD();
  else if (name == "EVWORD")		reg = fRcu.EVWORD();
  else if (name == "TRCFG2")		reg = fRcu.TRCFG2();
  else if (name == "FMIREG")		reg = fRcu.FMIREG();
  else if (name == "FMOREG")		reg = fRcu.FMOREG();
    
  // --- Instruction coder ---
  else if (name == "IRADD")		reg = 0;	// fRcu.IRADD();
  else if (name == "IRDAT")		reg = 0;	// fRcu.IRDAT();
    
  // --- Configuration registers ---
  else if (name == "ACTFEC")		reg = fRcu.ACTFEC();
  else if (name == "PMCFG")		reg = fRcu.PMCFG();
  else if (name == "ALTROIF")		reg = fRcu.ALTROIF();
  else if (name == "ALTROCFG1")		reg = fRcu.ALTROCFG1();
  else if (name == "ALTROCFG2")		reg = fRcu.ALTROCFG2();
    
  // --- Monitoring and safety registers ---
  else if (name == "INTREG")		reg = 0;	// fRcu.INTREG();
  else if (name == "RESREG")		reg = 0;	// fRcu.RESREG();
  else if (name == "ERRREG")		reg = 0;	// fRcu.ERRREG();
  else if (name == "INTMOD")		reg = fRcu.INTMOD();
  else if (name == "SCADD")		reg = 0;	// fRcu.SCADD();
  else if (name == "SCDAT")		reg = 0;	// fRcu.SCDAT();
    
  // --- Trigger & event interface ---
  else if (name == "TRCFG1")		reg = fRcu.TRCFG1();
  else if (name == "TRGCONF")		reg = fRcu.TRGCONF();
  else if (name == "RDOMOD")		reg = fRcu.RDOMOD();
  else if (name == "L1Timeout") 	reg = fRcu.L1Timeout();
  else if (name == "SWTRGCNT")		reg = 0;	// fRcu.SWTRGCNT();
  else if (name == "AUXTRGCNT")		reg = 0;	// fRcu.AUXTRGCNT();
  else if (name == "TTCL2ACNT")		reg = 0;	// fRcu.TTCL2ACNT();
  else if (name == "TTCL2RCNT")		reg = 0;	// fRcu.TTCL2RCNT();
  else if (name == "DSTBACNT")		reg = 0;	// fRcu.DSTBACNT();
  else if (name == "DSTBBCNT")		reg = 0;	// fRcu.DSTBBCNT();
  else if (name == "TRSFACNT")		reg = 0;	// fRcu.TRSFACNT();
  else if (name == "TRSFBCNT")		reg = 0;	// fRcu.TRSFBCNT();
  else if (name == "ACKACNT")		reg = 0;	// fRcu.ACKACNT();
  else if (name == "ACKBCNT")		reg = 0;	// fRcu.ACKBCNT();
  else if (name == "CSTBACNT")		reg = 0;	// fRcu.CSTBACNT();
  else if (name == "CSTBBCNT")		reg = 0;	// fRcu.CSTBBCNT();
  else if (name == "DSTBNUMA")		reg = 0;	// fRcu.DSTBNUMA();
  else if (name == "DSTBNUMB")		reg = 0;	// fRcu.DSTBNUMB();

  return reg;
}

//____________________________________________________________________
Rcuxx::RcuCommand*
RcuConf::Rcu::Name2Command(const std::string& name) 
{
  Rcuxx::RcuCommand* cmd = 0;
  // --- Error and status  ---
  if      (name == "RS_STATUS")		cmd = fRcu.RS_STATUS();
  else if (name == "CLEARRDRXREG")	cmd = fRcu.CLEARRDRXREG();
       
    
  // --- U2F Specific registers ---
  else if (name == "CLR_EVTAG")		cmd = 0; // fRcu.CLR_EVTAG();
  else if (name == "RDFM")		cmd = 0; // fRcu.RDFM();
  else if (name == "TRG_CLR")		cmd = 0; // fRcu.TRG_CLR();
  else if (name == "WRFM")		cmd = 0; // fRcu.WRFM();
    
  // --- Instruction coder ---
  else if (name == "ABORT")		cmd = 0; // fRcu.ABORT();
  else if (name == "EXEC")		cmd = fRcu.EXEC();
    
    
  // --- Configuration registers ---
  else if (name == "DCS_ON")		cmd = fRcu.DCS_ON();
  else if (name == "DDL_ON")		cmd = fRcu.DDL_ON();
  else if (name == "FECRST")		cmd = fRcu.FECRST();
  else if (name == "GLB_RESET")		cmd = fRcu.GLB_RESET();
  else if (name == "RCU_RESET")		cmd = fRcu.RCU_RESET();
  else if (name == "CONFFEC")		cmd = fRcu.CONFFEC();
  else if (name == "ARBITERIRQ")	cmd = fRcu.ARBITERIRQ();
    
  // --- Monitoring and safety registers ---
  else if (name == "RS_ERRREG")     	cmd = 0; // fRcu.RS_ERRREG();
  else if (name == "RS_RESREG")     	cmd = 0; // fRcu.RS_RESREG();
  else if (name == "SCEXEC")        	cmd = 0; // fRcu.SCEXEC();
  else if (name == "SCCOMMAND")		cmd = 0; // fRcu.SCCOMMAND();
    
  // --- Trigger & event interface ---
  else if (name == "L1_CMD")		cmd = fRcu.L1_CMD();
  else if (name == "L1_I2C")		cmd = fRcu.L1_I2C();
  else if (name == "L1_TTC")		cmd = fRcu.L1_TTC();
  else if (name == "RDABORT")		cmd = 0; // fRcu.RDABORT();
  else if (name == "RS_DMEM1")		cmd = fRcu.RS_DMEM1();
  else if (name == "RS_DMEM2")		cmd = fRcu.RS_DMEM2();
  else if (name == "RS_TRCFG")		cmd = fRcu.RS_TRCFG();
  else if (name == "RS_TRCNT")		cmd = fRcu.RS_TRCNT();
  else if (name == "SWTRG")		cmd = fRcu.SWTRG();

  return cmd;
}

//____________________________________________________________________
Rcuxx::RcuMemory*
RcuConf::Rcu::Name2Memory(const std::string& name) 
{
  Rcuxx::RcuMemory* mem = 0;
  // --- Error and status  ---
  // --- U2F Specific registers ---
  // --- Instruction coder ---
  if      (name == "IMEM")		mem = fRcu.IMEM();
  else if (name == "RMEM")		mem = 0; // fRcu.RMEM();
    
  // --- Configuration registers ---
  else if (name == "PMEM")		mem = fRcu.PMEM();
    
  // --- Monitoring and safety registers ---
  else if (name == "STATUS") 		mem = 0; // fRcu.STATUS();
    
  // --- Trigger & event interface ---
  else if (name == "DM1")		mem = 0; // fRcu.DM1();
  else if (name == "DM2")		mem = 0; // fRcu.DM2();
  else if (name == "ACL")		mem = fRcu.ACL();
  else if (name == "HEADER")		mem = fRcu.HEADER();

  return mem;
}

//____________________________________________________________________
unsigned int
RcuConf::Rcu::Write(const Parameter&   p, 
		    const SingleValue& v) 
{
  if (p.IsBlob()) return kInvalidValue;
  std::cout << "Check if '" << p.Name() << "' is for us" << std::endl;
  
  // Check if this is a register 
  Rcuxx::RcuRegister* reg  = Name2Register(p.Name());
  // We return negative error codes for the Rcu++ errors. 
  if (reg) {
    std::cout << "Writing " << reg->Name() << " to Rcu" << std::endl;
    reg->Decode(v.Values());
    return - reg->Commit();
  }
  
  std::cout << "Check if '" << p.Name() << "' is for us" << std::endl;
  // Check if this is a command 
  Rcuxx::RcuCommand*  cmd = Name2Command(p.Name());
  if (cmd) {
    std::cout << "Writing " << cmd->Name() << " to Rcu" << std::endl;
    cmd->SetArgument(v.Values());
    return - cmd->Commit();
  }
  return kUnknownParameter;
}
  
//____________________________________________________________________
unsigned int
RcuConf::Rcu::Write(const Parameter&   p, 
		    const BlobValue&   v) 
{
  if (!p.IsBlob()) return kInvalidValue;
  
  // Check if this is a memory
  Rcuxx::RcuMemory* mem = Name2Memory(p.Name());
  // We return negative error codes for the Rcu++ errors. 
  if (mem) {
    std::cout << "Writing " << mem->Name() << " to Rcu" << std::endl;
    std::vector<unsigned int> vals;
    v.Values(vals);
    mem->Set(0, vals.size(), &(vals[0]));
    return - mem->Commit(0, vals.size());
  }
  return kUnknownParameter;
}

//____________________________________________________________________
unsigned int
RcuConf::Rcu::Read(const Parameter&   p, 
		   SingleValue&       v) 
{
  if (p.IsBlob()) return kInvalidValue;
  
  // Check if this is a register 
  Rcuxx::RcuRegister* reg  = Name2Register(p.Name());
  // We return negative error codes for the Rcu++ errors. 
  if (reg) {
    unsigned ret = reg->Update(); // Should we do this?
    v.Set(reg->Encode()); 
    return - ret;
  }
  
  // Check if this is a command 
  Rcuxx::RcuCommand*  cmd = Name2Command(p.Name());
  if (cmd) {
    // v = cmd->Argument();
    return kSuccess;
  }
  return kUnknownParameter;
}
  
//____________________________________________________________________
unsigned int
RcuConf::Rcu::Read(const Parameter&   p, 
		   BlobValue&         v) 
{
  if (!p.IsBlob()) return kInvalidValue;
  
  // Check if this is a memory
  Rcuxx::RcuMemory* mem = Name2Memory(p.Name());
  // We return negative error codes for the Rcu++ errors. 
  if (mem) {
    std::vector<unsigned int> vals;
    unsigned int ret = mem->Update(); // Should we do this?
    v.Set(mem->Data()); 
    return ret;
  }
  return kUnknownParameter;
}

//____________________________________________________________________
void
RcuConf::Rcu::StartBlock()
{
  fRcu.StartBlock();
}

//____________________________________________________________________
int
RcuConf::Rcu::EndBlock()
{
  return fRcu.EndBlock();
}

//____________________________________________________________________
long int* 
RcuConf::Rcu::GetBlock() const
{
  const Rcuxx::Rcu::Block_t& block = fRcu.GetBlock();
  return const_cast<long int*>(&(block[0]));
}

//____________________________________________________________________
const std::string&
RcuConf::Rcu::ErrorString(int ret) 
{
  return fErrorString = fRcu.ErrorString(ret);
}

//____________________________________________________________________
bool
RcuConf::Rcu::Create(RcuDb::Server& s) 
{
  // Everything is in the RCU 
  Parameter::Where w = Parameter::kRcu;
  // Create throws an exception in case of errors
  try {
    /* === Registers === */
    // Component::Create(s, "ERRST",	w, false, 0x0); // Read-only
    // Component::Create(s, "TRCNT",	w, false, 0x0); // Read-only
    // Component::Create(s, "RDOFEC",	w, false, 0x0); // Read-only
    // Component::Create(s, "CHADD",	w, false, 0x0); // Read-only
    // Component::Create(s, "FWVERS",	w, false, 0x0); // Read-only
    // Component::Create(s, "BPVERS",	w, false, 0x0); // Read-only
    // Component::Create(s, "FECERRA",	w, false, 0x0); // Read-only
    // Component::Create(s, "FECERRB",	w, false, 0x0); // Read-only
    // Component::Create(s, "RDOERR",	w, false, 0x0); // Read-only
    // Component::Create(s, "RCUBUS",	w, false, 0x0); // Read-only
    // --- U2F Specific registers ---
    Component::Create(s, "LWADD",	w, false, 0xffffffff);
    Component::Create(s, "EVWORD",	w, false, 0xffffffff);
    Component::Create(s, "TRCFG2",	w, false, 0xffffffff);
    Component::Create(s, "FMIREG",	w, false, 0xffffffff);
    Component::Create(s, "FMOREG",	w, false, 0xffffffff);
    // --- Instruction coder ---
    // Component::Create(s, "IRADD",	w, false, 0x0); // Read-only
    // Component::Create(s, "IRDAT",	w, false, 0x0); // Read-only
    // --- Configuration registers ---
    Component::Create(s, "ACTFEC",	w, false, 0xffffffff);
    Component::Create(s, "PMCFG",	w, false, 0xffffffff);
    Component::Create(s, "ALTROIF",	w, false, 0xffffffff);
    Component::Create(s, "ALTROCFG1",	w, false, 0xffffffff);
    Component::Create(s, "ALTROCFG2",	w, false, 0xffffffff);
    // --- Monitoring and safety registers ---
    // Component::Create(s, "INTREG",	w, false, 0x0); // Read-only
    // Component::Create(s, "RESREG",	w, false, 0x0); // Read-only
    // Component::Create(s, "ERRREG",	w, false, 0x0); // Read-only
    Component::Create(s, "INTMOD",	w, false, 0xffffffff);
    // Component::Create(s, "SCADD",	w, false, 0x0); // Read-only
    // Component::Create(s, "SCDAT",	w, false, 0x0); // Read-only
    // --- Trigger & event interface ---
    Component::Create(s, "TRCFG1",	w, false, 0xffffffff);
    Component::Create(s, "TRGCONF",	w, false, 0xffffffff);
    Component::Create(s, "RDOMOD",	w, false, 0xffffffff);
    Component::Create(s, "L1Timeout",	w, false, 0xffffffff);
    // Component::Create(s, "SWTRGCNT",	w, false, 0x0); // Read-only
    // Component::Create(s, "AUXTRGCNT",	w, false, 0x0); // Read-only
    // Component::Create(s, "TTCL2ACNT",	w, false, 0x0); // Read-only
    // Component::Create(s, "TTCL2RCNT",	w, false, 0x0); // Read-only
    // Component::Create(s, "DSTBACNT",	w, false, 0x0); // Read-only
    // Component::Create(s, "DSTBBCNT",	w, false, 0x0); // Read-only
    // Component::Create(s, "TRSFACNT",	w, false, 0x0); // Read-only
    // Component::Create(s, "TRSFBCNT",	w, false, 0x0); // Read-only
    // Component::Create(s, "ACKACNT",	w, false, 0x0); // Read-only
    // Component::Create(s, "ACKBCNT",	w, false, 0x0); // Read-only
    // Component::Create(s, "CSTBACNT",	w, false, 0x0); // Read-only
    // Component::Create(s, "CSTBBCNT",	w, false, 0x0); // Read-only
    // Component::Create(s, "DSTBNUMA",	w, false, 0x0); // Read-only
    // Component::Create(s, "DSTBNUMB",	w, false, 0x0); // Read-only
    /* === Commands === */
    // --- Error and status  ---
    Component::Create(s, "RS_STATUS",	w, false, 0xffffffff);
    Component::Create(s, "CLEARRDRXREG",	w, false, 0xffffffff);
    // --- U2F Specific registers ---
    // Component::Create(s, "CLR_EVTAG",	w, false, 0x0); // Read-only
    // Component::Create(s, "RDFM",	w, false, 0x0); // Read-only
    // Component::Create(s, "TRG_CLR",	w, false, 0x0); // Read-only
    // Component::Create(s, "WRFM",	w, false, 0x0); // Read-only
    // --- Instruction coder ---
    // Component::Create(s, "ABORT",	w, false, 0x0); // Read-only
    Component::Create(s, "EXEC",	w, false, 0xffffffff);
    // --- Configuration registers ---
    Component::Create(s, "DCS_ON",	w, false, 0xffffffff);
    Component::Create(s, "DDL_ON",	w, false, 0xffffffff);
    Component::Create(s, "FECRST",	w, false, 0xffffffff);
    Component::Create(s, "GLB_RESET",	w, false, 0xffffffff);
    Component::Create(s, "RCU_RESET",	w, false, 0xffffffff);
    Component::Create(s, "CONFFEC",	w, false, 0xffffffff);
    Component::Create(s, "ARBITERIRQ",	w, false, 0xffffffff);
    // --- Monitoring and safety registers ---
    // Component::Create(s, "RS_ERRREG",	w, false, 0x0); // Read-only
    // Component::Create(s, "RS_RESREG",	w, false, 0x0); // Read-only
    // Component::Create(s, "SCEXEC",	w, false, 0x0); // Read-only
    // Component::Create(s, "SCCOMMAND",	w, false, 0x0); // Read-only
    // --- Trigger & event interface ---
    Component::Create(s, "L1_CMD",	w, false, 0xffffffff);
    Component::Create(s, "L1_I2C",	w, false, 0xffffffff);
    Component::Create(s, "L1_TTC",	w, false, 0xffffffff);
    // Component::Create(s, "RDABORT",	w, false, 0x0); // Read-onlyy
    Component::Create(s, "RS_DMEM1",	w, false, 0xffffffff);
    Component::Create(s, "RS_DMEM2",	w, false, 0xffffffff);
    Component::Create(s, "RS_TRCFG",	w, false, 0xffffffff);
    Component::Create(s, "RS_TRCNT",	w, false, 0xffffffff);
    Component::Create(s, "SWTRG",	w, false, 0xffffffff);
    /* === Memories === */
    Component::Create(s, "IMEM",	w, true,  0xffffffff);
    // Component::Create(s, "RMEM",	w, false, 0x0); // Invalid
    Component::Create(s, "PMEM",	w, true,  0xffffffff);
    // Component::Create(s, "STATUS",	w, false, 0x0); // Invalid
    // Component::Create(s, "DM1",	w, false, 0x0); // Invalid
    // Component::Create(s, "DM2",	w, false, 0x0); // Invalid
    Component::Create(s, "ACL",		w, true,  0xffffffff);
    Component::Create(s, "HEADER",	w, true,  0xffffffff);
  }
  catch (bool& e) {
    return e;
  }
  return true;
}


//____________________________________________________________________
//
// EOF
//
