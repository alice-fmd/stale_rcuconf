// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuconf.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Program that configures the front-end from DB look-ups,
    and using the Rcu++ abstraction layer.
*/
#include "config.h"
#include "Configurator.h"
#include <rcudb/Server.h>
#include <rcudb/Sql.h>
#include <rcuxx/Rcu.h>
#include <rcuxx/Bc.h>
#include <rcuxx/Fmd.h>
#include <rcuxx/Altro.h>
#include "Rcu.h"
#include "Bc.h"
#include "Fmd.h"
#include "Altro.h"
#include <stdexcept>
#include <iostream>
#include "Options.h"

/** Main function 
    @param argc Number of command line arguments
    @param argv Vector of command line arguments
    @return 0 on success, error code otherwise */
int
main(int argc, char** argv) 
{
  using namespace RcuConf;
  Option<bool>        hOpt('h', "help",    "Show this help", false,false);
  Option<bool>        VOpt('V', "version", "Show version number", false,false);
  Option<bool>        fOpt('f', "fmd",     "Use the FMD", false,false);
  Option<std::string> dOpt('d', "debug",   "Debugging output");
  Option<bool>        eOpt('e', "emul",    "Emulation output", false,false);
  Option<unsigned>    tOpt('t', "tag",     "Tag to use\t", 0);
  Option<unsigned>    xOpt('x', "x",       "X coordinate\t", 0);
  Option<unsigned>    yOpt('y', "y",       "Y coordinate\t", 0);
  Option<unsigned>    zOpt('z', "z",       "Z coordinate\t", 0);
  Option<std::string> cOpt('c', "db",      "Database connection url", 
			   "mysql://config@localhost/RCU");
  Option<std::string> uOpt('r', "rcu",     "Rcu++ connection url", 
			   "fee://localhost/FMD1");
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(cOpt);
  cl.Add(tOpt);
  cl.Add(xOpt);
  cl.Add(yOpt);
  cl.Add(zOpt);
  cl.Add(fOpt);
  cl.Add(uOpt);
  cl.Add(dOpt);
  cl.Add(eOpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << "rcuconf version " << VERSION << std::endl;
    return 0;
  }

  
  std::string con = cOpt;
  std::string url = uOpt;
  std::string deb = dOpt;
  try {
    // Connect to DB
    RcuDb::Server* server = RcuDb::Server::Connect(con);
    if (!server) 
      throw std::runtime_error("Failed to open connection to server!");

    // Connect to FEE
    Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(url.c_str(), eOpt.IsSet(), 
				       deb.find("rcu")!=std::string::npos);
    if (!rcu) 
      throw std::runtime_error("Failed to open connection to RCU");
    if (deb.find("backend") != std::string::npos) 
      rcu->SetDebug(Rcuxx::Rcu::kBackend, 1);
    
    
    Rcuxx::Bc* bc = 0;
    if (fOpt.IsSet()) bc = new Rcuxx::Fmd(*rcu);
    else              bc = new Rcuxx::Bc(*rcu);
    bc->SetDebug(deb.find("bc") != std::string::npos || 
		 (fOpt.IsSet() && deb.find("fmd") != std::string::npos));

    Rcuxx::Altro altro(*rcu);
    altro.SetDebug(deb.find("altro") != std::string::npos);

    // Make components 
    Rcu   rcuComp(*rcu);
    Bc*   bcComp = (fOpt.IsSet() ? new Fmd(*bc) : new Bc(*bc));
    Altro altroComp(altro);
    

    // Make configurator 
    Configurator configurator(*server, rcuComp, *bcComp, altroComp);
    configurator.SetDebug(deb.find("config") != std::string::npos);
    
    // Make the configuration 
    int ret = configurator.Write(tOpt, xOpt, yOpt, zOpt);
    if (ret < 0) 
      throw std::runtime_error(configurator.ErrorString());

    const Rcuxx::Rcu::Block_t& b = rcu->GetBlock();
    std::cout << "Block: " << std::endl;
    for (int i = 0; i < ret; i++){
      std::cout << "\t" << i << "\t0x" << std::setfill('0') 
		<< std::hex << std::setw(8) << b[i] << std::dec 
		<< std::setfill(' ') << std::endl;
    }
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}

//
// EOF
//

    
      
    
    


