// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    BlobValue.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Value table class
*/
#ifndef RCUCONF_BLOBVALUE_H
#define RCUCONF_BLOBVALUE_H
#ifndef RCUCONF_VALUE_H
# include <rcuconf/Value.h>
#endif
#ifndef RCUDB_BLOB_H
# include <rcudb/Blob.h>
#endif
#ifndef __LIST__
# include <list>
#endif
// Forward declarations 
namespace RcuDb 
{
  class Row;
  class Sql;
}

namespace RcuConf 
{
  class Parameter;
  class Config;
  
  /** @class BlobValue 
      @brief Values of parameters.  Contains up to a blob of data.
      How this is to be intepreted is up to the client 
      @ingroup Tables
  */
  class BlobValue : public Value
  {
  public:
    /** User constructor 
	@param config  Reference to Config table 
	@param param   Reference to Parameter table
	@param address Reference to Address table 
	@param value   Array of values (of type @a T) to store 
    */
    template <typename T>
    BlobValue(int                   config, 
	      int                   param, 
	      int                   address, 
	      const std::vector<T>& value) 
      : Value(config, param, address),
	fValue(value)
    {}
    /** Destructor */ 
    virtual ~BlobValue() { }

    /** Type of List of addresss */
    typedef std::list<BlobValue*> List;

    /** Print to standard out */ 
    virtual void Print(std::ostream& o=std::cout) const;
    /** Insert this object into the database 
	@param s Server to contact */
    virtual bool Insert(RcuDb::Server& s);
    /** Create a table for these objects 
	@param s Server to create the table in */
    static  bool Create(RcuDb::Server& s);
    /** Delete table for these objects 
	@param s Server to delete the table from */
    static  bool Drop(RcuDb::Server& s);
    /** Query the database for objects of this type 
	@param s Server to query
	@param l Return list 
	@param cond Optional condition
	@return List of objects of this type matching @a cond */
    static  bool Select(List& l, RcuDb::Server& s, const RcuDb::Sql& cond);
    /** Query the database for objects of this type 
	@param s Server to query
	@param l Return list.  The list is sorted according to the
	version number in descending order (highest version number first) 
	@param c Configuration entry to match 
	@param p Parameter entry to match. 
	@param addr  Whether to only look for broadcast values (value
	@f$< 0@f$), non-broadcast values in general (value = 0), or a
	specific address (value @f$> 0@f$). 
	@return List of objects of this type matching @a cond */
    static  bool Select(List& l, RcuDb::Server& s, 
			const Config&    c,
			const Parameter& p, 
			int addr=-1);
    /** Query the database for objects of this type 
	@param s Server to query
	@param l Return list.  The list is sorted according to the
	version number in descending order (highest version number first) 
	@param c Configuration entry to match 
	@param p Parameter entry to match. 
	@param addr  Whether to only look for broadcast values (value
	@f$< 0@f$), non-broadcast values in general (value = 0), or a
	specific address (value @f$> 0@f$). 
	@return List of objects of this type matching @a cond */
    static  bool Select(List& l, RcuDb::Server& s, int c, int p, 
			int addr=-1);

    /** @return Version number */
    template <typename T>
    void Values(std::vector<T>& v) const
    {
      fValue.Decode(v);
    }
    /** @param v Values to set */ 
    template <typename T> 
    void Set(const std::vector<T>& v) 
    {
      fValue.Encode(v);
    }
    /** Table name */
    static const std::string fgName;
  protected:
    /** Construct from a row 
	@param row Row to construct from */
    BlobValue(RcuDb::Row& row);
    /** Value of the parameter encoded as a blob integer. */
    RcuDb::Blob fValue;
    /** Base class is a friend */
    friend class Value;
  };
}
#endif
//
// EOF
//

