// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Component.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Implementation of base class for Component configurator classes
*/
#include "Component.h"
#include <rcuxx/Altro.h>
#include <rcuxx/altro/AltroCommand.h>
#include <rcuxx/altro/AltroRegister.h>
#include "SingleValue.h"
#include <rcudb/Server.h>
#include <rcudb/Row.h>
#include <rcudb/Sql.h>
// #include "BlobValue.h"

//====================================================================
void
RcuConf::Component::Create(RcuDb::Server& s, const std::string& name, 
			   Parameter::Where dest, bool isBlob, 
			   unsigned int mask) throw(bool)
{
  Parameter p(name, dest, isBlob, mask);
  if (p.Insert(s)) return;
  throw false;
}

//====================================================================
unsigned int
RcuConf::BusComponent::DoWrite(const Parameter&   p, 
			       const SingleValue& v, 
			       int board, unsigned int chip, 
			       unsigned int channel) 
{
  // std::cout << "Processing " << p.Name() << " with value " << v.Values()
  // << std::endl;
  if (p.IsBlob()) return kInvalidValue;
  
  // Check if this is a register 
  Rcuxx::AltroRegister* reg  = Name2Register(p.Name());
  // We return negative error codes for the Rcu++ errors. 
  if (reg) {
    if (board < 0) { 
      if (!reg->IsBroadcastable()) return kInvalidAddress;
      reg->SetBroadcast();
    }
    else
      reg->SetAddress(board, chip, channel);
    // TODO 
    reg->Decode(v.Values());
    return - reg->Commit();
  }
  
  // Check if this is a command 
  Rcuxx::AltroCommand*  cmd = Name2Command(p.Name());
  if (cmd) {
    if (board < 0) cmd->SetBroadcast();
    else           cmd->SetAddress(board, chip, channel);
    // cmd->SetArgument(v.Values());
    return - cmd->Commit();
  }
  return kUnknownParameter;
}

//____________________________________________________________________
unsigned int
RcuConf::BusComponent::DoRead(const Parameter&   p, 
			      SingleValue&       v,
			      int board, unsigned int chip, 
			      unsigned int channel) 
{
  
  if (p.IsBlob()) return kInvalidValue;
  
  // Check if this is a register 
  Rcuxx::AltroRegister* reg  = Name2Register(p.Name());
  // We return negative error codes for the Rcu++ errors. 
  if (reg) {
    if (board < 0 &&  !reg->IsBroadcastable()) return kInvalidAddress;
    unsigned ret = reg->Update(); // Should we do this?
    v.Set(reg->Encode()); 
    return - ret;
  }
  
  // Check if this is a command 
  Rcuxx::AltroCommand*  cmd = Name2Command(p.Name());
  if (cmd) {
    // v = cmd->Argument();
    return kSuccess;
  }
  return kUnknownParameter;
}

//____________________________________________________________________
//
// EOF
//
