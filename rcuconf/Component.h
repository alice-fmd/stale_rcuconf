// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Component.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of base class for Component configurator classes
*/
#ifndef RCUCONF_COMPONENT_H
#define RCUCONF_COMPONENT_H
#ifndef RCUCONF_PARAMETER
# include <rcuconf/Parameter.h>
#endif
#ifndef RCUCONF_ADDRESS
# include <rcuconf/Address.h>
#endif
#ifndef __STRING__
# include <string>
#endif

namespace Rcuxx
{
  class AltroRegister;
  class AltroCommand;
}

// Forward decls in DB namespace
namespace RcuDb 
{
  class Server;
}

namespace RcuConf
{
  /** @defgroup Components Component interfaces */

  // Forward decls. 
  class SingleValue;
  class BlobValue;
  class Address;
  
  
  /** @class Component 
      @brief Base class for component configurators 
      @ingroup Components 
  */
  class Component 
  {
  public:
    /** Constructor */
    Component() {}
    /** Destructor */
    virtual ~Component() {}
    
    /** Status codes.  Note, that negative errors correspond to Rcu++
	error codes.   */
    enum {
      /** Success */
      kSuccess = 0, 
      /** Operation not supported for this component */
      kNotSupported, 
      /** The parameter does not belong to this component */
      kUnknownParameter, 
      /** The value has wrong cardinality for the parameter */
      kInvalidValue, 
      /** Invalid address for this register (i.e., trying to broadcast
	  when it's not allowed. */
      kInvalidAddress,
      /** Other errors */
      kFailure 
    };
	
    /** Write Parameter @a p, with SingleValue @a v.  A call to this
	member function implies writing in broadcast. 
	@param p Parameter entry
	@param v Value entry
	@return 0 on success, error code otherwise */
    virtual unsigned int Write(const Parameter&   p, 
			       const SingleValue& v) = 0;
    /** Write Parameter @a p, with SingleValue @a v for the Address
	@a a. A call to this member function implies writing to a
	particular address on the bus. 
	@param p Parameter entry
	@param v Value entry
	@param a Address entry
	@return 0 on success, error code otherwise */
    virtual unsigned int Write(const Parameter&   p, 
			       const SingleValue& v, 
			       const Address&     a) { return kNotSupported; }
    /** Write Parameter @a p, with BlobValue @a v.  A call to this
	member function implies writing in broadcast. 
	@param p Parameter entry
	@param v Value entry
	@return 0 on success, error code otherwise */
    virtual unsigned int Write(const Parameter&   p, 
			       const BlobValue&   v) { return kNotSupported; }
    /** Write Parameter @a p, with BlobValue @a v for the Address
	@a a. A call to this member function implies writing to a
	particular address on the bus. 
	@param p Parameter entry
	@param v Value entry
	@param a Address entry
	@return 0 on success, error code otherwise */
    virtual unsigned int Write(const Parameter&   p, 
			       const BlobValue&   v, 
			       const Address&     a) { return kNotSupported; }

    /** Read Parameter @a p, with SingleValue @a v.  A call to this
	member function implies writing in broadcast. 
	@param p Parameter entry
	@param v Value entry
	@return 0 on success, error code otherwise */
    virtual unsigned int Read(const Parameter&   p, 
			      SingleValue&       v) = 0;
    /** Read Parameter @a p, with SingleValue @a v for the Address
	@a a. A call to this member function implies writing to a
	particular address on the bus. 
	@param p Parameter entry
	@param v Value entry
	@param a Address entry
	@return 0 on success, error code otherwise */
    virtual unsigned int Read(const Parameter&   p, 
			      const Address&     a,
			      SingleValue&       v) { return kNotSupported; }
    /** Read Parameter @a p, with BlobValue @a v.  A call to this
	member function implies writing in broadcast. 
	@param p Parameter entry
	@param v Value entry
	@return 0 on success, error code otherwise */
    virtual unsigned int Read(const Parameter&   p, 
			      BlobValue&         v) { return kNotSupported; }
    /** Read Parameter @a p, with BlobValue @a v for the Address
	@a a. A call to this member function implies writing to a
	particular address on the bus. 
	@param p Parameter entry
	@param v Value entry
	@param a Address entry
	@return 0 on success, error code otherwise */
    virtual unsigned int Read(const Parameter&   p, 
			      const Address&     a,
			      BlobValue&   	 v) { return kNotSupported; }
  protected:
    /** Create an entry in the parameter table.  
	@param s Server to contact
	@param name Name of the parameter
	@param dest Destination 
	@param isBlob Whether this parameter has blob value 
	@param mask Mask used for verification. 
	@throw bool of value false in case of errors. 
    */
    static void Create(RcuDb::Server& s, const std::string& name, 
		       Parameter::Where dest, bool isBlob, 
		       unsigned int mask) throw(bool);    
  };

  //__________________________________________________________________
  /** @class BusComponent 
      @brief a Component on the bus 
      @ingroup Components
  */
  class BusComponent : public Component 
  {
  public:
    /** Constructor */
    BusComponent() {}
    /** Destructor */
    virtual ~BusComponent() {}

    /** Write Parameter @a p, with SingleValue @a v.  A call to this
	member function implies writing in broadcast. 
	@param p Parameter entry
	@param v Value entry
	@return 0 on success, error code otherwise */
    virtual unsigned int Write(const Parameter&   p, 
			       const SingleValue& v) 
    {
      return DoWrite(p, v, -1, 0, 0);
    }
    /** Write Parameter @a p, with SingleValue @a v for the Address
	@a a. A call to this member function implies writing to a
	particular address on the bus. 
	@param p Parameter entry
	@param v Value entry
	@param a Address entry
	@return 0 on success, error code otherwise */
    virtual unsigned int Write(const Parameter&   p, 
			       const SingleValue& v, 
			       const Address&     a)
    {
      return DoWrite(p, v, a.Board(), a.Chip(), a.Channel());
    }

    /** Read Parameter @a p, with SingleValue @a v.  A call to this
	member function implies writing in broadcast. 
	@param p Parameter entry
	@param v Value entry
	@return 0 on success, error code otherwise */
    virtual unsigned int Read(const Parameter&   p, 
			      SingleValue&       v)
    {
      return DoRead(p, v, -1, 0, 0);
    }
    /** Read Parameter @a p, with SingleValue @a v for the Address
	@a a. A call to this member function implies writing to a
	particular address on the bus. 
	@param p Parameter entry
	@param v Value entry
	@param a Address entry
	@return 0 on success, error code otherwise */
    virtual unsigned int Read(const Parameter&   p, 
			      const Address&     a,
			      SingleValue&       v)
    {
      return DoRead(p, v, a.Board(), a.Chip(), a.Channel());
    }

  protected:
    /** Map a parameter name to a register */ 
    virtual Rcuxx::AltroRegister* Name2Register(const std::string& name) = 0;
    /** Map a parameter name to a command */ 
    virtual Rcuxx::AltroCommand* Name2Command(const std::string& name) = 0;
    /** Do the actual write.  The address/broadcast is set before, so
	only the parameter and the value is passed 
	@param p Parameter entry
	@param v Value entry
	@param board Board #
	@param chip Chip #
	@param channel Channel #
	@return 0 on success, error code otherwise */
    virtual unsigned int DoWrite(const Parameter&   p, 
				 const SingleValue& v, 
				 int board, unsigned int chip, 
				 unsigned int channel);
    /** Do the actual read.  The address/broadcast is set before, so
	only the parameter and the value is passed 
	@param p Parameter entry
	@param v Value entry
	@param board Board #
	@param chip Chip #
	@param channel Channel #
	@return 0 on success, error code otherwise */
    virtual unsigned int DoRead(const Parameter&   p, 
				SingleValue&       v,
				int board, unsigned int chip, 
				unsigned int channel);
  };
}
#endif
//
// EOF
//

  
