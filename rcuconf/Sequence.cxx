// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Sequence.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Sequence table class
*/
#include "Sequence.h"
#include <rcudb/Server.h>
#include <rcudb/Sql.h>
#include <rcudb/Row.h>
#include <rcudb/Result.h>
#include "Table.h"
#include <iostream>

//_____________________________________________________________________
int
RcuConf::Sequence::Increment(RcuDb::Server& server) 
{
  int ret = -1;

  // Get the rows in the sequence table 
  RcuDb::Sql sql1;
  sql1 << "SELECT " << (Table::IsDump() ? "@id:=id" : "*") << " FROM Sequence";
  if (Table::IsDump()) {
    RcuDb::Sql sql2;
    sql2 << "UPDATE Sequence SET id=@id+1";
    std::cout << sql1 << ";\n" << sql2 << ";" << std::endl;
    return true;
  }
  RcuDb::Result* res = server.Query(sql1);
  if (!res) return ret;

  // Loop through rows (there should be only one!)
  RcuDb::Row*    row = 0;
  while ((row = res->Next())) row->Field(0, ret);
  if (ret < 0) return ret;
  
  
  // Update the sequence table 
  RcuDb::Sql sql2;
  sql2 << "UPDATE Sequence SET id=" << ret + 1;
  if (!server.Exec(sql2)) return -1;

  // Increment and return 
  ret++;
  return ret;
}


//_____________________________________________________________________
bool
RcuConf::Sequence::Create(RcuDb::Server& server) 
{
  RcuDb::Sql sql;
  sql << "CREATE TABLE Sequence ("
      << "id INT KEY)";
  if (Table::IsDump()) { 
    RcuDb::Sql sql2;
    sql2 << "INSERT INTO Sequence SET id=0";
    std::cout << sql << ";\n" << sql2 << ";" << std::endl;
    return true;
  }
  
  RcuDb::Sql sql2;
  sql2 << "INSERT INTO Sequence SET id=0";
  bool ret = server.Exec(sql2);
  return ret;
}

//_____________________________________________________________________
bool
RcuConf::Sequence::Drop(RcuDb::Server& server) 
{
  RcuDb::Sql sql = "DROP TABLE Sequence";
  if (Table::IsDump()) {
    std::cout << sql << ";" << std::endl;
    return true;
  }
  return server.Exec(sql);
}

//_____________________________________________________________________
//
// EOF
//
