#include "config.h"
#include "Options.h"
#include "Sequence.h"
#include "Config.h"
#include "Priority.h"
#include "Parameter.h"
#include "SingleValue.h"
#include "BlobValue.h"
#include "Address.h"
#include <rcudb/Server.h>
#include <rcudb/Sql.h>
#include <iostream>
#include <sstream>
#include <stdexcept>

//____________________________________________________________________  
int 
main(int argc, char** argv)
{
  using namespace RcuConf;
  Option<bool>        hOpt('h', "help",     "Show this help", false,false);
  Option<bool>        VOpt('V', "version",  "Show version number",false,false);
  Option<unsigned>    tOpt('t', "tag",         "Tag to use", 0);
  Option<unsigned>    xOpt('x', "x",           "X coordinate", 0);
  Option<unsigned>    yOpt('y', "y",           "Y coordinate", 0);
  Option<unsigned>    zOpt('z', "z",           "Z coordinate", 0);
  Option<std::string> dOpt('d', "description", "Description", "");
  Option<std::string> pOpt('p', "priority",    "Priority to use", "default");
  Option<std::string> cOpt('c', "connection", "Database connection url", 
			   "mysql://config@localhost/RCU");
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(cOpt);
  cl.Add(tOpt);
  cl.Add(xOpt);
  cl.Add(yOpt);
  cl.Add(zOpt);
  cl.Add(dOpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << "createConfigs version " << VERSION << std::endl;
    return 0;
  }

  try {
    std::string con = cOpt;
    RcuDb::Server* server = RcuDb::Server::Connect(con);
    if (!server) 
      throw std::runtime_error("Failed to open connection to server!");

    Priority::List orders;
    RcuDb::Sql sel;
    sel << "description = '" << pOpt.Value() << "'";
    if (!Priority::Select(orders, *server, sel)) {
      std::stringstream s;
      s << "Failed to get priority '" << pOpt.Value() << "': " 
	<< server->ErrorString();
      throw std::runtime_error(s.str()); 
    }
    if (orders.size() < 0)
      throw std::runtime_error("Too few matches on default priority");
    if (orders.size() > 1) 
      throw std::runtime_error("Too many matches on default priority");
      
    Priority* priority = *(orders.begin());

    RcuConf::Config c(tOpt, xOpt, yOpt, zOpt, priority->Id(), dOpt);
    if (!c.Insert(*server)) {
      std::stringstream errstr;
      errstr << "Failed to insert " << dOpt.Value() << " into Config table: " 
	     << server->ErrorString();
      throw std::runtime_error(errstr.str());
    }
    
    Config::List confs;
    if (!Config::Select(confs, *server, "")) {
      std::stringstream s;
      s << "Failed to get configs: " << server->ErrorString();
      throw std::runtime_error(s.str());
    }    
    for (Config::List::iterator i = confs.begin(); i != confs.end(); ++i)
      (*i)->Print();
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}

      


