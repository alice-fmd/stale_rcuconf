// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Rcu configurator class
*/
#ifndef RCUCONF_RCU_H
#define RCUCONF_RCU_H
#ifndef RCUCONF_COMPONENT_H
# include <rcuconf/Component.h>
#endif

namespace Rcuxx
{
  class Rcu;
  class RcuRegister;
  class RcuCommand;
  class RcuMemory;
}

namespace RcuConf
{
  /** @class Rcu 
      @brief Component configurator for the RCU. 
      @ingroup Components 
   */
  class Rcu : public Component 
  {
  public: 
    /** Constructor 
	@param rcu Reference to low-level RCU interface */
    Rcu(Rcuxx::Rcu& rcu) : fRcu(rcu) {}
    /** Destructor */
    virtual ~Rcu() {}

    /** Write Parameter @a p, with SingleValue @a v.  A call to this
	member function implies writing in broadcast. 
	@param p Parameter entry
	@param v Value entry
	@return 0 on success, error code otherwise */
    unsigned int Write(const Parameter&   p, 
		       const SingleValue& v);
    /** Write Parameter @a p, with BlobValue @a v.  A call to this
	member function implies writing in broadcast. 
	@param p Parameter entry
	@param v Value entry
	@return 0 on success, error code otherwise */
    unsigned int Write(const Parameter&   p, 
		       const BlobValue&   v);

    /** Read Parameter @a p, with SingleValue @a v.  A call to this
	member function implies writing in broadcast. 
	@param p Parameter entry
	@param v Value entry
	@return 0 on success, error code otherwise */
    unsigned int Read(const Parameter&   p, 
		      SingleValue&       v);
    /** Read Parameter @a p, with BlobValue @a v.  A call to this
	member function implies writing in broadcast. 
	@param p Parameter entry
	@param v Value entry
	@return 0 on success, error code otherwise */
    unsigned int Read(const Parameter&   p, 
		      BlobValue&         v);

    /** Get the latest error string */
    const std::string& ErrorString(int err);

    /** Start a data block */ 
    void StartBlock();
    /** End a data block 
	@return number of words in block */
    int EndBlock();
    /** Get the data block 
	@return Data block */
    long int* GetBlock() const;
    
    
    /** Create entries for all parameters in the data base
	@param server Server to contact. 
	@return @c true on success, @c false otherwise */
    static bool Create(RcuDb::Server& server);
  protected:
    /** Map a parameter name to a register */ 
    Rcuxx::RcuRegister* Name2Register(const std::string& name);
    /** Map a parameter name to a command */ 
    Rcuxx::RcuCommand* Name2Command(const std::string& name);
    /** Map a parameter name to a memory */ 
    Rcuxx::RcuMemory* Name2Memory(const std::string& name);
    
    /** Reference to low-level interface */
    Rcuxx::Rcu& fRcu;
    /** Last error string */
    std::string fErrorString;
  };
}
#endif
//
// EOF
// 
