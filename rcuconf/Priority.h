// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Priority.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Priority table class
*/
#ifndef RCUCONF_PRIORITY_H
#define RCUCONF_PRIORITY_H
#ifndef RCUCONF_TABLE_H
# include <rcuconf/Table.h>
#endif
#ifndef RCUDB_BLOB_H
# include <rcudb/Blob.h>
#endif
#ifndef __LIST__
# include <list>
#endif

namespace RcuDb
{
  class Server;
  class Row;
  class Result;
  class Sql;
}

namespace RcuConf 
{
  // Forward declarations 
  class Config;

  /** @class Priority 
      @brief Priority to write a register to - if a specific chip should
      be ordered. 
      @ingroup Tables 
  */
  class Priority : public Table
  {
  public:
    /** User constructor 
	@param desc   Description
	@param params Priority of parameters */
    Priority(const std::string&      desc, 
	     const std::vector<int>& params) 
      : fDescription(desc), 
	fParams(params)
    {}
    /** Destructor */ 
    virtual ~Priority() { }

    /** Type of List of prioritys */
    typedef std::list<Priority*> List;

    /** Print to standard out */ 
    virtual void Print(std::ostream& o=std::cout) const;
    /** Insert this object into the database 
	@param s Server to contact */
    virtual bool Insert(RcuDb::Server& s);
    /** Create a table for these objects 
	@param s Server to create the table in */
    static  bool Create(RcuDb::Server& s);
    /** Delete table for these objects 
	@param s Server to delete the table from */
    static  bool Drop(RcuDb::Server& s);
    /** Query the database for objects of this type 
	@param s Server to query
	@param l Return list 
	@param cond Optional condition
	@return List of objects of this type matching @a cond */
    static  bool Select(List& l, RcuDb::Server& s, const RcuDb::Sql& cond);
    /** Query the database for objects of this type 
	@param s Server to query
	@param l Return list 
	@param c Config to get priority for 
	@return List of objects of this type matching @a cond */
    static  bool Select(List& l, RcuDb::Server& s, const Config& c);

    /** @return Board priority */
    const std::string& Description() const { return fDescription; }
    /** @param params Priority of parameters.  On return, this array is
	filed with references to rows in the Parameter table */
    void Params(std::vector<int>& params) const { fParams.Decode(params); }

    /** Table name */
    static const std::string fgName;
  protected:
    /** Construct from a row 
	@param row Row to construct from */
    Priority(RcuDb::Row& row);
    /** Description */ 
    std::string fDescription;
    /** Priority encoded into a blob */
    RcuDb::Blob fParams;
    
  };
}
#endif
//
// EOF
//

  
