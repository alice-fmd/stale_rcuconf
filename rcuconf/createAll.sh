#!/bin/sh
url=
fmd=

while test $# -gt 0 ; do 
    case $1 in 
	-c) url="-c $2" ; shift ;; 
	-f) fmd="-f"    ;;
	-h)
	    cat <<EOF
Usage: $0 [OPTIONS]

Options:
	-c URL		Connection url 
	-f		Do for the FMD 
	-h		This help 

URL is of the form 

	<scheme>://[<user>[:<passwd>]@][<host>]/<DB>[?<options>]

where <scheme> is one of 'mysql' or 'oracle', <user> is the database user
name to use, <passwd> is the password for user <user>, <host> is where the
database server is running, and <DB> is the name of the database.  Additional
database management system specific options to the connection can be in
<option>

This script first deletes all tables, (re)creates them, and then populates 
them with dummy values
EOF
	    exit 0
	    ;;
    *) echo "Unknown option '$1', try $0 -h" > /dev/stderr 
       exit 1
       ;;
    esac
    shift
done


set -e 
echo "URL: $url"

echo "=== Dropping tables ==================================" 
./dropTables $url
echo "=== Creating tables ==================================" 
./createTables $url
echo "=== Creating addresses ===============================" 
./createAddresses $url 
echo "=== Creating parameters ==============================" 
./createParams $url $fmd
echo "=== Creating priority ================================" 
./createPriority $url 
echo "=== Creating configs =================================" 
./createConfigs $url -t 0 
./createConfigs $url -t 1
./createConfigs $url -t 1
echo "=== Creating values ==================================" 
./createValues $url -t 1


echo "=== Done =============================================" 
