#include <iostream>
#include <string>
#include <sstream>


int 
main(int argc, char** argv)
{
  std::string s = "FMD-FEE_0_0_0";
  if (argc > 1) s = argv[1];
  
  std::string::size_type us = s.find('_');
  if (us == std::string::npos) {
    std::cerr << "Didn't find a '_' in \"" << s << "\"" << std::endl;
    return 1;
  }
  
  std::string nums = s.substr(us+1, s.size() - us);
  std::cout << "Numbers in " << nums << std::endl;
  std::stringstream str(nums);
  int x, y, z;
  char us1, us2;
  str >> x >> us1 >> y >> us2 >> z;
  if (str.bad()) 
    std::cerr << "Failed reading numbers" << std::endl;
  if (us1 != '_' || us2 != '_') 
    std::cerr << "failed to read underscores: '" << us1 << "' '" << us2 
	      << "'" << std::endl;
  
#if 0
  for (size_t i = 0; i < 3; i++) {
    int& c   = (i == 0 ? x : (i == 1 ? y : z));
    bool rus = (i == 0 || i == 2);
    str >> c;
    if (str.bad()) {
      std::cerr << "Failed while reading coordinate # " << i 
		<< ": " << str.str() << std::endl;
      return 1;
    }
    if (i == 0 || i == 1) {
      str >> us;
      if (str.bad()) {
	std::cerr << "Failed while reading '_'" 
		  << ": " << str.str() << std::endl;
      }
    }
  }
#endif
  std::cout << "x=" << x << " y=" << y << " z=" << z << std::endl;
  return 0;
}

