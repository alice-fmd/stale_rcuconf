#include "config.h"
#include "CommandCoderBase.h"
#include "Options.h"
#include <iostream>
#include <iomanip>

int
main(int argc, char** argv) 
{
  Option<bool>        hOpt('h', "help",    "Show this help", false,false);
  Option<bool>        VOpt('V', "version", "Show version number", false,false);
  Option<unsigned>    tOpt('t', "tag",     "Tag to use\t", 0);
  Option<std::string> TOpt('T', "target",  "Feeserver target","FMD-FEE_0_0_0");
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(tOpt);
  cl.Add(TOpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << "rcuconf version " << VERSION << std::endl;
    return 0;
  }
 
  CommandCoderBase* coco = CommandCoderBase::createInstance();
  

  int nwords = coco->createDataBlock(const_cast<char*>(TOpt->c_str()), tOpt);
  if (nwords < 0) {
    typedef std::vector<std::string> errors_t;
    errors_t errs = coco->getError();
    std::cerr << "Errors: " << std::endl;
    for (errors_t::iterator i = errs.begin(); i != errs.end(); ++i) 
      std::cerr << "\t" << *i << std::endl;
    return 0;
  }
  
  long int* block = coco->getDataBlock();
  std::cout << "Block: " << std::endl;
  for (int i = 0; i < nwords; i++) 
    std::cout << "\t"   << i 
	      << "\t0x" << std::hex << std::setfill('0') 
	      << std::setw(sizeof(long)) << block[i] 
	      << std::setfill(' ') << std::dec << std::endl;
    
  return 0;
}

      
