// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Table.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Configurator class
*/
#ifndef RCUCONF_TABLE_H
#define RCUCONF_TABLE_H
#ifndef __STRING__
# include <string>
#endif
#include <iosfwd>

namespace RcuDb
{
  class Server;
  class Row;
  class Sql;
}
namespace std
{
  extern ostream cout;
}

namespace RcuConf 
{

  /** @class Table Table.h <rcuconf/Table.h>
      @brief Base class  for table entries 
      @ingroup Tables
  */
  class Table 
  {
  public:
    /** Destructor */
      virtual ~Table() {}
    /** Insert entry into table 
	@param server Server to contact
	@return @c true on success, @c false otherwise */
    virtual bool Insert(RcuDb::Server& server) = 0;
    /** Get the database id  
	@return the database id. If not set, returns -1 */
    int Id() const { return fId; }
    /** Helper function to drop this table 
	@param server Server to contact
	@param table  Name of the table 
	@return @c true on success, @c false otherwise */
    static bool Drop(RcuDb::Server& server, const std::string& table);
    /** Print a table 
	@param out Output stream */ 
    virtual void Print(std::ostream& o=std::cout) const {}
    /** Set whether we should dump to standard out rather than making
	the actual stuff 
	@param dump If @c true, then dump to standard out. */ 
    static void Mode(bool dump) { fDump = dump; }
    static bool IsDump() { return fDump; }
  protected:
    /** Constructor */
    Table() : fId(-1) {}
    /** Copy constructor */ 
    Table(const Table& table) : fId(table.fId) {}
    /** Construct from a row */ 
    Table(RcuDb::Row& row);
    /** Make a unique id for this entry 
	@param server Server to contact */ 
    bool MakeId(RcuDb::Server& server);
    /** Insert the id (or variable) into query @a sql */
    RcuDb::Sql& IdOut(RcuDb::Sql& sql);
    /** Possibly dump query to standard out */ 
    static bool Dump(const RcuDb::Sql& sql);
    /** Database id */ 
    int fId;
    /** Wether to dump inserts and creates to standard out rather than
	execute the commands on the server.  */
    static bool fDump;
  };
}

#endif
//
// EOF
//

    
