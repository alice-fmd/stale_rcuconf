// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT CommandCoder interface 
//
/** @file    CommandCoder.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of CommandCoder interface 
*/
#ifndef RCUCONF_COMMANDCODER_H
#define RCUCONF_COMMANDCODER_H
#ifndef RCUCONF_COMMANDCODERBASE_H
# include <rcuconf/CommandCoderBase.h>
#endif
#ifndef RCUCONF_CONFIGURATOR_H
# include <rcuconf/Configurator.h>
#endif
#ifndef __STRING__
# include <string>
#endif
#ifndef __VECTOR__
# include <vector>
#endif

namespace Rcuxx
{
  class Rcu;
  class Bc;
  class Altro;
}

namespace RcuDb 
{
  class Server;
}

namespace RcuConf 
{
  // Forward declarations 
  class Rcu;
  class Bc;
  class Altro;
  class Configurator;

  /** @class CommandCoder 
      @brief Command coder used by the @b InterCom @b Layer to
      configure the front-end electronics. 
      @ingroup Steer 
      The class is a thin wrapper around the Configurator class.  It
      simply forwards requests to the internal Configurator object. 

      The class is a singleton, meaning there can be only one object
      of this class at any given point.  

      The database connection is chosen at configure time by the @c
      configure option @c --with-datbase. 

      The @b Rcu++ connection is always the @c coder: connection. 
   */
  class CommandCoder : public CommandCoderBase 
  {
  public:
    /** Constructor */
    CommandCoder();
    /** Destructor */
    virtual ~CommandCoder() {}
    /** 
     * Create a data block 
     * @param target Where to send it 
     * @param tag The tag 
     * @return  Error code 
     */
    virtual int createDataBlock(char *target, int tag);
    /** 
     * Get the command block 
     * @return  data block 
     */
    virtual long int * getDataBlock();
    /** 
     * Get list of error strings 
     * @return  List of error  strings 
     */
    virtual std::vector<std::string> getError() { return fErrors; }
  protected:
    /** initialize the command coder */ 
    void init();
    /** Read configuration file 
	@return @c true on success, @c false otherwise */
    bool readConfig();
    /** Database server */ 
    RcuDb::Server* fServer;
    /** Rcu++ Rcu connection */ 
    Rcuxx::Rcu* fRcuxx;
    /** Rcu++ BC connection */ 
    Rcuxx::Bc* fBcxx;
    /** Rcu++ ALTRO connection */
    Rcuxx::Altro* fAltroxx;
    /** Rcu component */
    Rcu* fRcu;
    /** Bc Component */
    Bc* fBc;
    /** Altro component */
    Altro* fAltro;
    /** Configurator */
    Configurator* fConfigurator;
    /** Return value */ 
    int fRet;
    /** Error strings */ 
    std::vector<std::string> fErrors;
    /** Database url */
    std::string fDbUrl;
    /** RCU url */
    std::string fRcuUrl;
    /** Verbosity */
    bool fVerbose;
    /** Debug */ 
    bool fDebug;
    /** Whether we've been initialized */
    bool fInit;
    /** Detector */
    std::string fDetector;
  };

  /** Set the configuration file for the CommandCoder.  
      @note This function @e must be called before the InterCom Layer
      calls any member functions of the CommandCoder.
      @param file File to read configuration file 
  */
  void setConfigFile(const std::string& file);
}

#endif
//
// EOF
//


