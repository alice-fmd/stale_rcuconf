// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Altro.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Altro configurator class
*/
#include "Altro.h"
#include <rcuxx/Altro.h>
#include <rcuxx/altro/AltroADEVL.h>
#include <rcuxx/altro/AltroBCTHR.h>
#include <rcuxx/altro/AltroCommand.h>
#include <rcuxx/altro/AltroDPCF2.h>
#include <rcuxx/altro/AltroDPCFG.h>
#include <rcuxx/altro/AltroERSTR.h>
#include <rcuxx/altro/AltroKLCoeffs.h>
#include <rcuxx/altro/AltroPMADD.h>
#include <rcuxx/altro/AltroPMDTA.h>
#include <rcuxx/altro/AltroRegister.h>
#include <rcuxx/altro/AltroTRCFG.h>
#include <rcuxx/altro/AltroTRCNT.h>
#include <rcuxx/altro/AltroVFPED.h>
#include <rcuxx/altro/AltroZSTHR.h>
#include <rcudb/Row.h>
#include <rcudb/Sql.h>

//____________________________________________________________________
Rcuxx::AltroRegister*
RcuConf::Altro::Name2Register(const std::string& name) 
{
  Rcuxx::AltroRegister* reg = 0;
  if 	  (name == "ADEVL")	reg = 0; // Read-only fAltro.ADEVL(); 
  else if (name == "BCTHR")	reg = fAltro.BCTHR();
  else if (name == "DPCF2")	reg = fAltro.DPCF2();
  else if (name == "DPCFG")	reg = fAltro.DPCFG();
  else if (name == "ERSTR")	reg = 0; // Read-only fAltro.ERSTR();
  else if (name == "K1")	reg = fAltro.K1();
  else if (name == "K2")	reg = fAltro.K2();
  else if (name == "K3")	reg = fAltro.K3();
  else if (name == "L1")	reg = fAltro.L1();
  else if (name == "L2")	reg = fAltro.L2();
  else if (name == "L3")	reg = fAltro.L3();
  else if (name == "PMADD")	reg = fAltro.PMADD();
  else if (name == "PMDTA")	reg = fAltro.PMDTA();
  else if (name == "TRCFG")	reg = fAltro.TRCFG();
  else if (name == "TRCNT")	reg = 0; // Read-only fAltro.TRCNT();
  else if (name == "VFPED")	reg = fAltro.VFPED();
  else if (name == "ZSTHR")	reg = fAltro.ZSTHR();

  return reg;
}

//____________________________________________________________________
Rcuxx::AltroCommand*
RcuConf::Altro::Name2Command(const std::string& name) 
{
  Rcuxx::AltroCommand* cmd = 0;
  if      (name == "CHRDO")	cmd = fAltro.CHRDO();
  else if (name == "ERCLR")	cmd = fAltro.ERCLR();
  else if (name == "RPINC")	cmd = fAltro.RPINC();
  else if (name == "SWTRG")	cmd = fAltro.SWTRG();
  else if (name == "TRCLR")	cmd = fAltro.TRCLR();
  else if (name == "WPINC")	cmd = fAltro.WPINC();
  return cmd;
}

//____________________________________________________________________
bool
RcuConf::Altro::Create(RcuDb::Server& s) 
{
  // Everything is in the RCU 
  Parameter::Where w = Parameter::kAltro;
  // Create throws an exception in case of errors
  try {
    /* === Registers === */
    // Component::Create(s, "ADEVL",	w, false, 0xffffffff); // Read-only
    Component::Create(s, "BCTHR",	w, false, 0xffffffff);
    Component::Create(s, "DPCF2",	w, false, 0xffffffff);
    Component::Create(s, "DPCFG",	w, false, 0xffffffff);
    // Component::Create(s, "ERSTR",	w, false, 0xffffffff); // Read-only
    Component::Create(s, "K1",		w, false, 0xffff);
    Component::Create(s, "K2",		w, false, 0xffff);
    Component::Create(s, "K3",		w, false, 0xffff);
    Component::Create(s, "L1",		w, false, 0xffff);
    Component::Create(s, "L2",		w, false, 0xffff);
    Component::Create(s, "L3",		w, false, 0xffff);
    Component::Create(s, "PMADD",	w, false, 0x3ff);
    Component::Create(s, "PMDTA",	w, false, 0x3ff);
    Component::Create(s, "TRCFG",	w, false, 0xffffffff);
    // Component::Create(s, "TRCNT",	w, false, 0xffffffff); // Read-only
    Component::Create(s, "VFPED",	w, false, 0x3ff);
    Component::Create(s, "ZSTHR",	w, false, 0xffffffff);
    /* === Commands === */
    Component::Create(s, "CHRDO",	w, false, 0xffffffff);
    Component::Create(s, "ERCLR",	w, false, 0xffffffff);
    Component::Create(s, "RPINC",	w, false, 0xffffffff);
    Component::Create(s, "SWTRG",	w, false, 0xffffffff);
    Component::Create(s, "TRCLR",	w, false, 0xffffffff);
    Component::Create(s, "WPINC",	w, false, 0xffffffff);
  }
  catch (bool& e) {
    return e;
  }
  return true;
}


//____________________________________________________________________
//
// EOF
//
