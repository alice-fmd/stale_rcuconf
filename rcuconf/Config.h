// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Config.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Config table class
*/
#ifndef RCUCONF_CONFIG_H
#define RCUCONF_CONFIG_H
#ifndef RCUCONF_TABLE_H
# include <rcuconf/Table.h>
#endif
#ifndef __LIST__
# include <list>
#endif

namespace RcuDb 
{
  class Row;
  class Sql;
}

namespace RcuConf 
{
  /** @defgroup Tables Classes that correspond to tables */ 
  
  /** @class Config Config.h <rcuconf/Config.h>
      @brief Representation of entries in the @e Config table 
      @ingroup Tables
  */
  class  Config : public Table
  {
  public:
    /** User constructor 
	@param tag      Tag 
	@param x        X coordinate
	@param y        Y coordinate 
	@param z        Z coordinate 
	@param priority Reference to order table
	@param desc     Description  */
    Config(int tag, 
	   int x, 
	   int y, 
	   int z, 
	   int priority, 
	   const std::string& desc) 
      : fTag(tag), 
	fX(x), 
	fY(y),
	fZ(z), 
	fPriorityId(priority),
	fVersion(-1),
	fDescription(desc)
    {}
    /** Destructor */ 
    virtual ~Config() { }

    /** Type of List of configs */
    typedef std::list<Config*> List;

    /** Print to standard out */ 
    virtual void Print(std::ostream& o=std::cout) const;
    /** Insert this object into the database 
	@param s Server to contact */
    virtual bool Insert(RcuDb::Server& s);
    /** Create a table for these objects 
	@param s Server to create the table in */
    static  bool Create(RcuDb::Server& s);
    /** Delete table for these objects 
	@param s Server to delete the table from */
    static  bool Drop(RcuDb::Server& s);
    /** Query the database for objects of this type 
	@param s Server to query
	@param l Return list 
	@param cond Optional condition
	@return List of objects of this type matching @a cond */
    static  bool Select(List& l, RcuDb::Server& s, const RcuDb::Sql& cond);
    /** Query the database for objects of this type 
	@param s   Server to query
	@param l   Return list.  The list is ordered by the version
	number in descending order (highest version number first).  
	@param tag Tag to search for 
	@param x   X coordinate 
	@param y   Y coordinate 
	@param z   Z coordinate 
	@return true on success, false otherwise */
    static  bool Select(List& l, RcuDb::Server& s, int tag, int x, int y, int z);

    /** @return Tag identifier */ 
    int Tag() const { return fTag; }
    /** @return X coordinate */
    int X() const { return fX; }
    /** @return Y coordinate */
    int Y() const { return fY; }
    /** @return Z coordinate */
    int Z() const { return fZ; }
    /** @return Reference to priority */
    int PriorityId() const { return fPriorityId; }
    /** @return Version number */
    int Version() const { return fVersion; }
    /** @return Description */

    const std::string& Description() const { return fDescription; }  

    /** Table name */
    static const std::string fgName;
  protected:
    /** Construct from a row 
	@param row Row to construct from */
    Config(RcuDb::Row& row);
    /** Tag */
    int         fTag;
    /** X coordinate */ 
    int         fX;
    /** Y coordinate */ 
    int         fY;
    /** Z coordinate */ 
    int         fZ;
    /** Reference to priority */ 
    int         fPriorityId;
    /** Version number */
    int         fVersion;
    /** Description */
    std::string fDescription;  
  };
}

#endif
//
// EOF
//

  
