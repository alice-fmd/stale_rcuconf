// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Priority.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Priority table class
*/
#include "Priority.h"
#include "Config.h"
#include "Sequence.h"
#include <rcudb/Server.h>
#include <rcudb/Result.h>
#include <rcudb/Sql.h>
#include <rcudb/Row.h>
#include <iostream>

const std::string RcuConf::Priority::fgName("Priority");

//_____________________________________________________________________
RcuConf::Priority::Priority(RcuDb::Row& row) 
  : Table(row)
{
  row.Field(1, fDescription);
  row.Field(2, fParams);
}

//_____________________________________________________________________
void
RcuConf::Priority::Print(std::ostream& o) const
{
  o << "Priority: id="     << fId 
    << "\tdescription=" << fDescription 
    << "\tparams="      << fParams << std::endl;
}

//_____________________________________________________________________
bool
RcuConf::Priority::Insert(RcuDb::Server& server) 
{
  // Get a unique ID from the sequence table 
  if (!MakeId(server)) return false;

  // And then do the insert. 
  RcuDb::Sql sql2;
  sql2 << "INSERT INTO " << fgName << " VALUES(";
  IdOut(sql2) << "'" 
	      << fDescription << "','" 
	      << fParams      << "')";
  if (!Dump(sql2)) return true;
  return server.Exec(sql2);
}


//_____________________________________________________________________
bool
RcuConf::Priority::Create(RcuDb::Server& server) 
{
  RcuDb::Sql sql;
  sql << "CREATE TABLE " << fgName << " ("
      << "  id          INT KEY, " 
      << "  description VARCHAR(64) NOT NULL, " 
      << "  params      BLOB, "
      << "  INDEX(description))";
  if (!Dump(sql)) return true;
  return server.Exec(sql);
}

//_____________________________________________________________________
bool
RcuConf::Priority::Drop(RcuDb::Server& server) 
{
  return Table::Drop(server, fgName);
}

//_____________________________________________________________________
bool
RcuConf::Priority::Select(List& l, RcuDb::Server& server, const RcuDb::Sql& cond) 
{
  // Make the query 
  RcuDb::Sql     sql;
  sql << "SELECT * FROM " << fgName 
      << (cond.Text().empty() ? "" : " WHERE ") 
      << (cond.Text().empty() ? "" : cond);
  if (!Dump(sql)) return true;

  // std::cout << "Doing query: " << sql.Text() << std::endl;
  // Execute query, and check error status
  RcuDb::Result* res = server.Query(sql);
  if (server.IsError()) return false;

  // Make the result table if any 
  if (!res) return true;
  // res->Print();
  
  RcuDb::Row* row = 0;
  while ((row = res->Next())) l.push_back(new Priority(*row));

  // delete result, and return OK.
  delete res;
  return true;  
}

//_____________________________________________________________________
bool
RcuConf::Priority::Select(List& l, RcuDb::Server& server, const Config& c)
{
  RcuDb::Sql sql;
  sql << "id=" << c.PriorityId();
  return Select(l, server, sql);
}

//_____________________________________________________________________
//
// EOF
//


  

  
