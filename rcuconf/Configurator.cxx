// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Configurator.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Implementation of Configurator class
*/
#include "Configurator.h"
#include "Config.h"
#include "Priority.h"
#include "Parameter.h"
#include "SingleValue.h"
#include "BlobValue.h"
#include "Address.h"
#include <rcudb/Server.h>
#include <rcudb/Sql.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <algorithm>
#include "Rcu.h"
#include "Bc.h"
#include "Altro.h"

//____________________________________________________________________
RcuConf::Configurator::Configurator(RcuDb::Server& server,
				    Rcu&        rcu, 
				    Bc&         bc,
				    Altro&      altro)
  : fServer(server),
    fRcu(rcu),
    fBc(bc), 
    fAltro(altro),
    fErrorString(""), 
    fCurrent(0), 
    fOrder(0), 
    fDebug(false)
{}

//____________________________________________________________________
int
RcuConf::Configurator::Write(int           tag, 
			     int           x, 
			     int           y, 
			     int           z) 
{
  if (!GetCurrent(tag, x, y, z)) return -1;
  if (!GetOrder())               return -1;
  if (!GetParams())              return -1;

  // Start a configuration block 
  fRcu.StartBlock();

  // std::clog << "# Parameters: " << fParams.size() << std::endl;
  for (Parameter::List::iterator p = fParams.begin(); p != fParams.end(); ++p){
    Component* c = 0;
    std::string name;
    switch ((*p)->Destination()) {
    case Parameter::kRcu:    c = &fRcu;   name="Rcu";   break;
    case Parameter::kBc:     c = &fBc;    name="Bc";    break;
    case Parameter::kAltro:  c = &fAltro; name="Altro"; break;
    default:                 c = 0;       break;
    }
    if (!c) {
      std::stringstream s;
      s << "Configurator: Invalid component: " << (*p)->Destination();
      fErrorString = s.str();
      return -1;
    }
    // For testing, we ignore the RCU commands 
    if (name == "Rcu") continue;
    if (fDebug) 
      std::clog << "Processing " << name << " parameter " << (*p)->Name() 
		<< std::endl;
    if (!Write(*c, *(*p))) return -1;
  }
  return fRcu.EndBlock();
}

//____________________________________________________________________
bool
RcuConf::Configurator::GetCurrent(int           tag, 
				  int           x, 
				  int           y, 
				  int           z) 
{
  if (tag < 0) return false;
  if (fCurrent) delete fCurrent;
  fCurrent = 0;
  
  // Try specified configuration, fall-back for this RCU, and general
  // fall-back. 
  bool         ret      = true;
  int          tags[]   = { tag, 0, 0 };
  int          coords[] = { x, y, z, x, y, z, 0, 0, 0 };
  Config::List confs;
  for (size_t i = 0; i < 3; i++) {
    ret = Config::Select(confs, fServer, tags[i],
			 coords[3*i+0], coords[3*i+1], coords[3*i+2]);
    if (ret && confs.size() > 0) break;
    if (fServer.IsError()) {
      std::stringstream s;
      s << "Configurator: Failed to get configuration for tag=" 
	<< tag << " (x,y,z)=(" << x << "," << y << "," << z 
	<< ") or any of the fall-back configurations: "
	<< fServer.ErrorString();
      fErrorString = s.str();
      return false;
    }
  }

  // Check that we actually something back 
  if (confs.size() < 1) {
    fErrorString = "Configurator: Didn't get any configuration";
    return false;
  }

  // Get the identifier of the configuration 
  Config::List::iterator iter = confs.begin();
  fCurrent                    = *iter;
  ++iter;

  // Clean up result set 
  for (; iter != confs.end(); ++iter) delete *iter;

  if (fDebug) {
    std::clog << "Current config:\n\t" << std::flush;
    fCurrent->Print(std::clog);
  }
  
  return ret;
}
  
//____________________________________________________________________
bool
RcuConf::Configurator::GetOrder()
{
  if (fOrder) delete fOrder;
  fOrder = 0;

  // Try the specified order 
  bool           ret = false;
  Priority::List priors;
  for (size_t i = 0; i < 2; i++) {
    if (i == 0) ret = Priority::Select(priors,fServer,*fCurrent);
    else        ret = Priority::Select(priors,fServer,"description='default'");
    if (ret && priors.size() > 0) break;
    
    if (fServer.IsError()) {
      std::stringstream s;
      s << "Failed to get " << (i == 0 ? " " : " default ") 
	<< "priority list";
      if (i == 0) s << " for configuration " << fCurrent->Id();
      s << ": " << fServer.ErrorString();
      fErrorString = s.str();
      return false;
    }
  }
  if (priors.size() <= 0) {
    fErrorString = "Configurator: Didn't get any priority list";
    return false;
  }

  // Get the identifier of the Priority list
  Priority::List::iterator iter = priors.begin();
  fOrder                        = *iter;
  ++iter;

  // Clean up result set 
  for (; iter != priors.end(); ++iter) delete *iter;

  if (fDebug) {
    std::clog << "Current priority list:\n\t" << std::flush;
    fOrder->Print(std::clog);
  }

  return ret;
}
  
  
//____________________________________________________________________
bool
RcuConf::Configurator::GetParams()
{
  for (Parameter::List::iterator i = fParams.begin(); i != fParams.end(); ++i)
    delete (*i);
  fParams.clear();
  
  // Get the parameters we need 
  std::vector<int> order;
  fOrder->Params(order);
  RcuDb::Sql parSel;
  for (std::vector<int>::iterator i = order.begin(); i != order.end(); ++i){
    if (i != order.begin()) parSel << " OR";
    parSel << " id=" << *i;
  }
    
  // Get the parameters 
  if (!Parameter::Select(fParams, fServer, parSel)) {
    std::stringstream s;
    s << "Configurator: Failed to get parameters: " << fServer.ErrorString();
    fErrorString = s.str();
    return false;
  }
  return true;
}

//____________________________________________________________________
bool
RcuConf::Configurator::CheckWrite(int ret, 
				  const std::string& name, 
				  int addr) 
{
  if (ret == 0) return true;

  std::stringstream s;
  s << "Configurator: Failed to write " << (addr < 0 ? "broadcast " : "") 
    << "value of " << name;
  if (addr >= 0) s << " to address 0x" << std::hex << addr << std::dec;
  s << " - ";
  if (ret < 0) 
    s << "Error from Rcu++: " << fRcu.ErrorString(-ret);
  else {
    switch (ret) {
    case Component::kNotSupported:
      s << "Operation not supported for this component"; break;
    case Component::kUnknownParameter:
      s << "The parameter does not belong to this component"; break;
    case Component::kInvalidValue:
      s << "The value has wrong cardinality for the parameter"; break;
    case Component::kInvalidAddress:
      s << "Invalid address for this register";
    case Component::kFailure:
      s << "Other errors"; break;
    default: 
      s << "Uknown error"; break;
    }
  }
  fErrorString = s.str();
  if (fDebug) std::clog << fErrorString << std::endl;
  return false;
}

  
//____________________________________________________________________
bool
RcuConf::Configurator::Write(Component& c, Parameter& p) 
{
  int         confs[] = { fCurrent->Id(), 0 };
  int         pid     = p.Id();
  bool        ret     = true;
  std::string tab     = (p.IsBlob() ? BlobValue::fgName : SingleValue::fgName);
  Value::List bl;
  
  // std::clog << "Processing parameter " << p.Name() << std::endl;
  // First, we try to get the broadcast values 
  for (size_t i = 0; i < 2; i++) {
    ret = Value::Select(bl, fServer, tab, confs[i], pid, -1);
    if (ret && bl.size() > 0) break;
    if (fServer.IsError()) {
      std::stringstream s;
      s << "Configurator: Failed to get broadcast values for parameter " 
	<< p.Name() << " for configuration " << fCurrent->Id();
      fErrorString = s.str();
      return ret;
    }
  }
  // std::clog << "Got back " << bl.size() << " matches" << std::endl;
  
  // Try to write newest broadcast value 
  if (bl.size() > 0) {
    Value* v = *(bl.begin());
    int ret = 0;
    if (p.IsBlob()) { 
      BlobValue* b = static_cast<BlobValue*>(v);
      ret = c.Write(p, *b);
    }
    else {
      SingleValue* s = static_cast<SingleValue*>(v);
      ret = c.Write(p, *s);
    }
    if (!CheckWrite(ret, p.Name(), -1)) return false;
  }
  
  Value::List ll;
  // Then, try to get he local values
  for (size_t i = 0; i < 2; i++) {
    ret = Value::Select(ll, fServer, tab, confs[i], pid, 0);
    if (ret && ll.size() > 0) break;
    if (fServer.IsError()) {
      std::stringstream s;
      s << "Configurator: Failed to get local values for parameter " 
	<< p.Name() << " for configuration " << fCurrent->Id();
      fErrorString = s.str();
      return ret;
    }
  }
  
  // Loop over the local variables 
  int last = -1;
  for (Value::List::iterator i = ll.begin(); i != ll.end(); ++i) {
    if ((*i)->AddressId() == last) continue;
    last = (*i)->AddressId();
    
    // Try to get the address from the database 
    Address::List al;
    if (!Address::Select(al, fServer, *(*i))) {
      std::stringstream s;
      s << "Configurator: Failed to get address for value of parameter " 
	<< p.Name() << " for configuration " << fCurrent->Id();
      fErrorString = s.str();
      return ret;
    }
    // This is silently ignored
    if (al.size() <= 0) continue;
    Address* a   = *(al.begin());
    int ret = 0;
    Value* v = (*i);
    if (p.IsBlob()) { 
      BlobValue* b = static_cast<BlobValue*>(v);
      ret = c.Write(p, *b, *a);
    }
    else {
      SingleValue* s = static_cast<SingleValue*>(v);
      ret = c.Write(p, *s, *a);
    }
    if (!CheckWrite(ret, p.Name(), a->RawValue())) return false;
  }
  return true;
}

//____________________________________________________________________
//
// EOF
//
