// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Sequence.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Sequence table class
*/
#ifndef RCUCONF_SEQUENCE_H
#define RCUCONF_SEQUENCE_H

namespace RcuDb 
{
  class Row;
  class Server;
  class Sql;
}
namespace RcuConf 
{
  
  /** @namespace Sequence Sequence.h <rcuconf/Sequence.h>
      @brief Representation of entries in the @e Sequence table 
      @ingroup Tables
  */
  namespace Sequence 
  {
    /** Create sequence table 
	@param server Server to contact 
	@return @c true on success */
    bool Create(RcuDb::Server& server);
    /** Drop sequence table 
	@param server Server to contact 
	@return @c true on success */
    bool Drop(RcuDb::Server& server);
    /** Update sequence 
	@param server Server to contact
	@return New value of sequence */
    int  Increment(RcuDb::Server& server);
  };
}

#endif
//
// EOF
//
