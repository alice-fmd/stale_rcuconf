#include "config.h"
#include "Options.h"
#include "Sequence.h"
#include "Config.h"
#include "Priority.h"
#include "Parameter.h"
#include "SingleValue.h"
#include "BlobValue.h"
#include "Address.h"
#include <rcudb/Server.h>
#include <rcudb/Result.h>
#include <rcudb/Sql.h>
#include <iostream>
#include <sstream>
#include <stdexcept>

//____________________________________________________________________
template <typename T>
void dropTable(RcuDb::Server& s, const std::string& name) 
{
  if (T::Drop(s)) return;
  std::stringstream errstr;
  errstr << "Failed to drop the " << name << " table: " 
	 << s.ErrorString();
  std::cerr << errstr.str() << std::endl;
  // throw std::runtime_error(errstr.str());
}

//____________________________________________________________________
int 
main(int argc, char** argv)
{
  using namespace RcuConf;
  Option<bool>        hOpt('h', "help",    "Show this help", false,false);
  Option<bool>        VOpt('V', "version", "Show version number", false,false);
  Option<bool>        dOpt('d', "dump",    "Dump to standard out", false,false);
  Option<std::string> cOpt('c', "connection", "Database connection url", 
			   "mysql://config@localhost/RCU");
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(cOpt);
  cl.Add(dOpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << "raw2root version " << VERSION << std::endl;
    return 0;
  }

  RcuConf::Table::Mode(dOpt.IsSet());
  try {
    std::string con = cOpt;
    RcuDb::Server* server = 0;
    if (!dOpt.IsSet()) {
      server = RcuDb::Server::Connect(con);
      if (!server) 
	throw std::runtime_error("Failed to open connection to server!");

      RcuDb::Result* res = server->Query("SHOW TABLES");
      if (!res) {
	std::stringstream s;
	s << "Failed to get tables: " << server->ErrorString();
	std::cerr << s.str() << std::endl;
	// throw std::runtime_error(s.str());
      }
      res->Print();
    }
    
    dropTable<Address>(*server,     "Address");
    dropTable<BlobValue>(*server,   "BlobValue");
    dropTable<SingleValue>(*server, "SingleValue");
    dropTable<Parameter>(*server,   "Parameter");
    dropTable<Priority>(*server,       "Priority");
    dropTable<Config>(*server,      "Config");
    if (!Sequence::Drop(*server)) {
      std::stringstream errstr;
      errstr << "Failed to drop Sequence table: " 
	     << server->ErrorString() << std::endl;
      std::cerr << errstr.str() << std::endl;
      // throw std::runtime_error(errstr.str());
    }
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
    
  return 0;
}

      


