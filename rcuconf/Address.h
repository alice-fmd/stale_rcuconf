// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Address.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Address table class
*/
#ifndef RCUCONF_ADDRESS_H
#define RCUCONF_ADDRESS_H
#ifndef RCUCONF_TABLE_H
# include <rcuconf/Table.h>
#endif
#ifndef __LIST__
# include <list>
#endif

namespace RcuDb 
{
  class Row;
  class Sql;
}


namespace RcuConf 
{
  // Forward declarations 
  class Value;

  /** @class Address 
      @brief Address to write a register to - if a specific chip should
      be addressed. 
      @ingroup Tables 
  */
  class Address : public Table
  {
  public:
    /** User constructor 
	@param board Board address 
	@param chip  Chip address 
	@param channel Channel address */
    Address(int board, int chip=0, int channel=0) 
      : fAddress(((board   & 0x1F) << 7) +
		 ((chip    & 0x07) << 4) + 
		 ((channel & 0x0F)))
    {}
    /** Destructor */ 
    virtual ~Address() { }

    /** Type of List of addresss */
    typedef std::list<Address*> List;

    /** Print to standard out */ 
    virtual void Print(std::ostream& o=std::cout) const;
    /** Insert this object into the database 
	@param s Server to contact */
    virtual bool Insert(RcuDb::Server& s);
    /** Create a table for these objects 
	@param s Server to create the table in */
    static  bool Create(RcuDb::Server& s);
    /** Delete table for these objects 
	@param s Server to delete the table from */
    static  bool Drop(RcuDb::Server& s);
    /** Query the database for objects of this type 
	@param s Server to query
	@param l Return list 
	@param cond Optional condition
	@return List of objects of this type matching @a cond */
    static  bool Select(List& l, RcuDb::Server& s, const RcuDb::Sql& cond);
    /** Query the database for objects of this type 
	@param s Server to query
	@param l Return list 
	@param v Value to get addresses for 
	@return List of objects of this type matching @a cond */
    static  bool Select(List& l, RcuDb::Server& s, const Value& v);

    /** @return Board address */
    int Board() const { return (fAddress >> 7) & 0x1f; }
    /** @return Chip address */
    int Chip() const { return (fAddress >> 4) & 0x7; }
    /** @return Channel address */
    int Channel() const { return (fAddress & 0xf); }
    /** @return Get raw value */
    int RawValue() const { return fAddress; }
  protected:
    /** Construct from a row 
	@param row Row to construct from */
    Address(RcuDb::Row& row);
    /** Address encoded into a 32 bit integer */
    int fAddress;
  };
}

#endif
//
// EOF
//

  
