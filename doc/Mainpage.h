/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Title page documentation. */
/** @mainpage ALICE RCU Configuration

    @section intro Introduction 
    
    This package provides a C++ class library for configuring the RCU,
    BC, and ALTROs. 

    @section setup Setup 
    
    You should have a database server running.  Either a MySQL or an
    Oracle database manager.   You should create your RCU database
    in that server, and set up the needed user(s). 

    Then, you should configure the package according to your setup,
    for example
    @verbatim
    ./configure --with-db-url=mysql://config:secret@fmddb.cern.ch/RCU
    @endverbatim 
    
    You should ofcourse substitute your database server host name,
    database user name and password, and database name. 

    If you'd like the RcuConf::CommandCoder to directly configure the
    hardware, instead of going via the @b InterCom @b Layer (but you
    still want the @b InterCom @b Layer to make the request), pass an
    option like 
    @verbatim 
    --with-rcuxx-url=fee://dim.dns.node/fee_server 
    @endverbatim 
    (see also the <a
    href="http://fmd.nbi.dk/fmd/fee/rcuxx/rcuxx"><b>Rcu++</b></a>
    documentation). 

    If you want to use the library for configuring the FMD FEE, you
    should pass the option 
    @verbatim 
    --enable-fmd
    @endverbatim 
    (a similar option can be implemented for the PHOS, EMCAL, etc.). 

    Next, build and install the package 
    @verbatim 
    make 
    make install 
    @endverbatim 

    In the source tree, in the sub-directory @c rcuconf you will find
    a number of programs called @c create@e something.  This will set
    up tables, and fill them with example values.  You can run all of
    them by doing 
    @verbatim 
    (cd rcuconf && ./createAll.sh -c database_url)
    @endverbatim 
    where you should substitute @e database_url with the URL you've
    setup previously. 

    The installed program @c rcuconf allows you to directly configure
    the front-end from the command line, using any form of
    communication with the RCU supported by your @b Rcu++
    installation.  For example, to configure the RCU connected via a
    DDL on the local host you can do 
    @verbatim 
    rcuconf -r ddl:0.0 -c mysql://config:secret@fmddb.cern.ch/RCU
    @endverbatim 
    
    - @subpage design
    - @subpage schema 
*/

#error Not for compilation
//
// EOF
//
