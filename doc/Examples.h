/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Title page documentation. */

/** @example rcuconf.cxx 
    @par Example program to do configurations using
    RcuConf::Configurator 
*/
/** @example createTables.cxx 
    @par Create all tables in database 
*/
/** @example dropTables.cxx 
    @par Drop (delete) all tables in database 
*/
/** @example createPriority.cxx 
    @par Create an example priority list
*/
/** @example createConfigs.cxx 
    @par Create an example configuration
*/
/** @example createParams.cxx 
    @par Create all parameters in the database
*/
/** @example createAddresses.cxx 
    @par Create all addresses in the database
*/
/** @example createValues.cxx 
    @par Create example values of all parameters in the database
*/
#error Not for compilation 
/*
 * EOF
 */
