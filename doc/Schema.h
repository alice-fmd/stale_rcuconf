/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   Schema.h
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Title page documentation. */

/** @page schema Database Schema

    @image html schema.png 

    The schema of the data base consists of the tables 

    - @b Sequence This implements a @e sequence.  That is, a table
      with only row, and a number that is incremented.  This is used
      to make unique identifiers in the database. 
    - @b Config This table servers as a collection of configuration
      information.  It has a @e tag and @e coordinate field as
      selected by the InterCom Layer or the like.  It is versioned,
      meaning that for a given set of @e tag and @e coordinates, there
      can be more that one version.  It has a reference to a row in
      the @b Order table. 
    - @b Order Specifies in which order the parameters should be
      pushed onto the front end electronics.  It references the @b
      Parameter table. 
    - @b Parameter  Information about the parameters.  This table is
      fairly static.  Each row has a @e name which identifies the
      parameter, a flag that says whether the parameter is a @e RCU,
      @e BC, or @e ALTRO parameter, and a flag @e isblob indicating
      whether it has singular or blob value.  If the @e isblob is
      true, then the values of the parameters are stored in the @b
      BlobValue table.  Otherwise, the parameters values are stored in
      the @b SingleValue table. 
    - @b Value Technically this is not a table. It is in the diagram
      only to show the common fields of the @b SingleValue and @b
      BlobValue tables.  It has a reference to a particular
      configuration (a row in the @b Config table) and to a parameter
      (a row in the @b Parameter table).   It is versioned, meaning
      that one can have many (different) values for the same
      configuration. 
    - @b SingleValue This table stores singular values (a 32bit
      integer) of registers. The meaning of the value is left to
      the client to make. 
    - @b BlobValue This table stores array values (of whatever size) of
      a register or memory.  The meaning of the values is left to the
      client to make. 
    - @b Address This table stores destination addresses for the
      values.  If a row in the @b Value table does not have any rows
      in this table, then the value should only be written in
      broadcast.  The strategy is to select rows in the @b Value
      table, and find corresponding addresses in this table.  Then,
      values with no address is written first, and then values with
      addresses are writen next.  In this way, values are only written
      if they differ from the default (broadcast) value. 

   For each of these tables, there's a corresponding class in the
   RcuConf namespace, with the notable exception of the @b Sequence
   table.  Each class have static member function to create and drop
   the table from the database.  They also have static member
   functions to query the database for rows of the table.  Finally,
   objects of the tables can be inserted into the table via a simple
   member function.  The Insert member function of the tables ensures
   that a new unique identifier is obtained from the @b Sequence
   table, and that version numbers are updated (if applicable). 
*/

#error Not for compilation
//
// EOF
//
