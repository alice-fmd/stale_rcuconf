/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   Design.h
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Title page documentation. */

/** @page design Design

    @section design_overview Overview 
    
    The library consists of 3 parts: 

    - @ref design_schema 
    - @ref design_component
    - @ref design_steer 

    There is also a group of utility classes.

    @subsection design_schema Schema

    The schema it self, and the tables in it is described elsewhere
    (@ref schema).  

    For each table in the schema is a separate class (@ref Tables).
    Objects of these classes correspond to rows in the tables.  There
    is also class member functions that allows one to create and drop
    the tables, query the tables, insert values, and so on.  Some
    often used, specialised queries have been defined.

    Note, that the basic premisis is that no data should ever be
    deleted or overwritten.  For this reason, some of tables are @e
    versioned.  The versioning is automatic when using the provided
    insertion member functions. 

    If a differnt schema from what is implemented here, is required,
    one can easily define that by making new table classes. 

    @subsection design_component  Components 

    For each physical component in the system (@b RCU, @b BC/FMDD, and
    @b ALTRO) is a sperate class that will take values from the
    database and put them to the hardware.  This is done using the
    <a href="http://fmd.nbi.dk/fmd/fee/#rcuxx"><b>Rcu++</b></a>
    abstraction layer.  

    Using the @b Rcu++ abstraction layer enables us to write
    configuration code that can be reused no matter how we communicate
    with the RCU.  The concrete communication is selected at run-time
    via a simple URL-like string.  
    @code 
    Rcuxx::Rcu* rcu = Rcuxx::Open(url);
    @endcode 

    The @a url can be many things.  For example 
    @verbatim 
    fee://dim.dns.node/fee_server_name 
    fed://dim.dns.node/fed_server_name:fee_server_name 
    ddl:card:client 
    @endverbatim 

    For more on this, please refer to the @b Rcu++ documentation.  

    Each of the component classes (RcuConf::Rcu,
    RcuConf::Bc / RcuConf::Fmd, and RcuConf::Altro) takes a parameter
    description, a value, and a possible address, and uses @b Rcu++
    calls to encode the data.  So, for example, the RcuConf::Bc class
    makes calls to @c Rcuxx::Bc member functions. 

    If a different schema is chosen for the database tables, the
    component classes should be updated to reflect this.  

    @subsection design_steer Steering 

    A number of classes (@ref Steer) defines steering routines.  

    The class RcuConf::Configurator takes as input a logical
    configuration tag, and @a x,y,z coordinates, looks up the
    appropriate parameters and values in the database, and ships it
    off to the appropriate component handler. 

    If the configuration was not found, the default configuration is
    used instead. 

    Which parameters are chosen, depends on the data in Priority row
    corresponding to the configuration chosen by @a tag and the @a
    x,y,z coordinates passed.   The priority table rows specifies
    which parameters, and in which order the registers, commands, and
    memories are written to the electronics.  If no Priority row is
    specified, or it isn't found, then the default order is used. 

    The values selected depends on the configuration found, and the
    parameter.  For memories, the datablock is encoded into a string
    of 32 bit hexadecimal numbers stored as a string.  If no value
    corresponding to the configration was found, then the default
    value is used.  

    For each parameter, the RcuConf::Configurator class first finds
    values to be broadcased, and then ships those to the front.  Then
    it finds values for specific component addresses, and ships those
    to the front end electronics. 

    The class RcuConf::CommandCoder is used by the InterCom Layer.
    It, in turn, uses the RcuConf::Configurator interface with a
    special Rcu connection to build up blobs of data to be shipped to
    the front end.  

    Alternatively, the CommandCoder could take directly with the
    front-end by simply using a different Rcu connection. 

    So, the RcuConf::Configurator class is what one could use to do
    direct configuration (for example via the DDL), and the
    CommandCoder is used for the indirect configuration via the
    InterCom Layer.  Note, that RcuConf::CommandCoder uses the
    RcuConf::Configurator class to do the actual work. Like this, we
    define the work process in one place only. 
*/
#error Not for compilation 
/*
 * EOF
 */
