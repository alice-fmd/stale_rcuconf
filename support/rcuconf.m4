dnl -*- mode: Autoconf -*- 
dnl
dnl  
dnl  ROOT generic rcuconf framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_RCUCONF([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUCONF],
[
    AC_REQUIRE([AC_RCUDB])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcuconf-prefix],
        [AC_HELP_STRING([--with-rcuconf-prefix],
		[Prefix where Rcuconf is installed])],
        rcuconf_prefix=$withval, rcuconf_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcuconf-url],
        [AC_HELP_STRING([--with-rcuconf-url],
		[Base URL where the Rcuconf dodumentation is installed])],
        rcuconf_url=$withval, rcuconf_url="")
    if test "x${RCUCONF_CONFIG+set}" != xset ; then 
        if test "x$rcuconf_prefix" != "x" ; then 
	    RCUCONF_CONFIG=$rcuconf_prefix/bin/rcuconf-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(RCUCONF_CONFIG, rcuconf-config, no)
    rcuconf_min_version=ifelse([$1], ,0.11,$1)
    
    # Message to user
    AC_MSG_CHECKING(for Rcuconf version >= $rcuconf_min_version)

    # Check if we got the script
    rcuconf_found=no    
    if test "x$RCUCONF_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       RCUCONF_CPPFLAGS=`$RCUCONF_CONFIG --cppflags`
       RCUCONF_INCLUDEDIR=`$RCUCONF_CONFIG --includedir`
       RCUCONF_LIBS=`$RCUCONF_CONFIG --libs`
       RCUCONF_LTLIBS=`$RCUCONF_CONFIG --ltlibs`
       RCUCONF_LIBDIR=`$RCUCONF_CONFIG --libdir`
       RCUCONF_LDFLAGS=`$RCUCONF_CONFIG --ldflags`
       RCUCONF_LTLDFLAGS=`$RCUCONF_CONFIG --ltldflags`
       RCUCONF_PREFIX=`$RCUCONF_CONFIG --prefix`

       # Check the version number is OK.
       rcuconf_version=`$RCUCONF_CONFIG -V` 
       rcuconf_vers=`echo $rcuconf_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       rcuconf_regu=`echo $rcuconf_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $rcuconf_vers -ge $rcuconf_regu ; then 
            rcuconf_found=yes
       fi
    fi
    AC_MSG_RESULT($rcuconf_found - is $rcuconf_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_RCUCONF, [Whether we have rcuconf])

    if test "x$rcuconf_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS -L$RCUCONF_LIBDIR $RCUCONF_LIBS"
    	CPPFLAGS="$CPPFLAGS $RCUCONF_CPPFLAGS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_rcuconf_channel_h=0
        AC_CHECK_HEADER([rcuconf/Configurator.h], 
	                [have_rcuconf_configurator_h=1])

        # Check the library. 
        have_librcuconf=no
        AC_MSG_CHECKING(for -lrcuconf)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <rcuconf/Sequence.h>
#include <rcudb/Server.h>],
                        [RcuDb::Server* server = RcuDb::Server::Connect("");
                         RcuConf::Sequence::Create(*server);])], 
                        [have_librcuconf=yes])
        AC_MSG_RESULT($have_librcuconf)

        if test $have_rcuconf_configurator_h -gt 0    && \
            test "x$have_librcuconf"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_RCUCONF)
        else 
            rcuconf_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
    fi

    AC_MSG_CHECKING(where the Rcuconf documentation is installed)
    if test "x$rcuconf_url" = "x" && \
	test ! "x$RCUCONF_PREFIX" = "x" ; then 
       RCUCONF_URL=${RCUCONF_PREFIX}/share/doc/rcuconf/html
    else 
	RCUCONF_URL=$rcuconf_url
    fi	
    AC_MSG_RESULT($RCUCONF_URL)
   
    if test "x$rcuconf_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUCONF_URL)
    AC_SUBST(RCUCONF_PREFIX)
    AC_SUBST(RCUCONF_CPPFLAGS)
    AC_SUBST(RCUCONF_INCLUDEDIR)
    AC_SUBST(RCUCONF_LDFLAGS)
    AC_SUBST(RCUCONF_LIBDIR)
    AC_SUBST(RCUCONF_LIBS)
    AC_SUBST(RCUCONF_LTLIBS)
    AC_SUBST(RCUCONF_LTLDFLAGS)
])

dnl
dnl EOF
dnl 
